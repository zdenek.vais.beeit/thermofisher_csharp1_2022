﻿using System.ComponentModel;
using BeeIT.RefAndValue.App;


static void ModifyValue(int a)
{
    a++;
    Console.WriteLine(a);
}

static void ModifyValueByRef(ref int a)
{
    a++;
    Console.WriteLine(a);
}

static void ModifyReferenceType(User user)
{
    user.Name = "Pepa";
    Console.WriteLine(user.Name);
}

static void ModifyStructure(UserStruct user)
{
    user.Name = "Pepa";
    Console.WriteLine(user.Name);
}


Console.WriteLine("Predani hodnotoveho typu");
int a = 5;
Console.WriteLine(a);
ModifyValue(a);
Console.WriteLine(a);


Console.WriteLine();
Console.WriteLine("Predani hodnotoveho typu referenci");
a = 5;
Console.WriteLine(a);
ModifyValueByRef(ref a);
Console.WriteLine(a);


Console.WriteLine();
Console.WriteLine("Predani referencniho typu - tridy");
var user = new User
{
    Name = "Zdenek",
    Age = 42
};
Console.WriteLine(user.Name);
ModifyReferenceType(user);
Console.WriteLine(user.Name);


Console.WriteLine();
Console.WriteLine("Predani slozeneho hodnotoveho typu - struktury");
var userStruct = new UserStruct
{
    Name = "Zdenek",
    Age = 42
};
Console.WriteLine(userStruct.Name);
ModifyStructure(userStruct);
Console.WriteLine(userStruct.Name);


Console.ReadLine();

