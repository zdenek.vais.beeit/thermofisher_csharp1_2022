﻿namespace BeeIT.RefAndValue.App
{
    internal struct UserStruct
    {
        public string Name { get; set; }

        public int Age { get; set; }

    }
}
