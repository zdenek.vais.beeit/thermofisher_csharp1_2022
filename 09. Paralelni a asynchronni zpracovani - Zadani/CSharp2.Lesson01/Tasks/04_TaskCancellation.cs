﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace BeeIT.Parallel1.Tasks;

public static class TaskCancellationExample
{
    public static void Run()
    {
        var cancellationTokenSource = new CancellationTokenSource();


        var task = new Task(() => InfiniteLoop(cancellationTokenSource.Token));
        task.Start();
        Thread.Sleep(3000);

        cancellationTokenSource.Cancel();

    }


    private static void InfiniteLoop(CancellationToken cancellationToken)
    {
        while (true)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                return;
            }

                

            Console.WriteLine("Working");
            Thread.Sleep(500);
        }
    }
}