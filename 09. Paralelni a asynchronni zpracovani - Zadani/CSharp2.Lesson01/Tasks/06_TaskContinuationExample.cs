﻿using System;
using System.Threading.Tasks;

namespace BeeIT.Parallel1.Tasks;

public static class TaskContinuationExample
{
    public static void Method()
    {
        var task1 = Task.Run(() =>
        {
            return 42;
        });
            
        var task2 = task1.ContinueWith(task =>
        {
            return $"Mocnina {task.Result} je {task.Result * task.Result}";
        });


    }




























    public static void Basic()
    {
        Task<string> task1 = Task.Run(() =>
        {
            return 12;
        }).ContinueWith((antecedent) =>
        {
            return $"The Square of {antecedent.Result} is: {antecedent.Result * antecedent.Result}";
        });

        Console.WriteLine(task1.Result);
    }

    public static void Complicated()
    {
        Task<int> task = Task.Run(() =>
        {
            throw new Exception();
            return 10;
        });

        task.ContinueWith((i) =>
        {
            Console.WriteLine("TasK Canceled");
        }, TaskContinuationOptions.OnlyOnCanceled);
        task.ContinueWith((i) =>
        {
            Console.WriteLine("Task Faulted");
        }, TaskContinuationOptions.OnlyOnFaulted);


        var completedTask = task.ContinueWith((i) =>
        {
            Console.WriteLine("Task Completed");
        }, TaskContinuationOptions.OnlyOnRanToCompletion);

        try
        {
            completedTask.Wait();
        }
        catch (AggregateException)
        { }
    }

}