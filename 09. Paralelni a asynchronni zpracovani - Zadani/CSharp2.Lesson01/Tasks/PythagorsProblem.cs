﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp2.Lesson01.Tasks
{
    class PythagorsProblem
    {
        public static volatile int count = 0;

        public static void Pythagoras()
        {
            for (int a = 0; a < 500; a = a + 4)
            {
                var task = Task.Run(() => FindSolutionForA(a));
                var task1 = Task.Run(() => FindSolutionForA(a + 1));
                var task2 = Task.Run(() => FindSolutionForA(a + 2));
                var task3 = Task.Run(() => FindSolutionForA(a + 3));

                Task.WaitAll(task)
            }
        }

        private static void FindSolutionForA(int a)
        {
            for (int b = 0; b < 500; b++)
            {
                for (int c = 0; c < 500; c++)
                {
                    // Calculation
                    double result = Math.Sqrt(Math.Pow(a, 2) + Math.Pow(b, 2) + Math.Pow(c, 2));

                    if (result % 1 == 0)
                    {
                        count++;
                    }
                }
            }
        }
    }
}
