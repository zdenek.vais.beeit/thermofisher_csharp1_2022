﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace BeeIT.Parallel1.Tasks;

public class TaskSyntax
{
    public static void Creation()
    {
        var task = new Task(() =>
        {
            for (int i = 1; i < 1000000; i = i + 2)
            {
                Console.WriteLine($"Liche: {i}");
            }
        });
        task.Start();



        for (int i = 2; i < 1000000; i = i + 2)
        {
            Console.WriteLine($"Sude: {i}");
        }
    }

    public static void CalculateSum()
    {
        var hugeArray = getHugeArray();
            
        var task1 = Task.Run(() => calculate(hugeArray, 0, hugeArray.Length / 3));
        var task2 = Task.Run(() => calculate(hugeArray, hugeArray.Length / 3 + 1, 2*hugeArray.Length / 3));
        var task3 = Task.Run(() => calculate(hugeArray, 2* hugeArray.Length / 3 + 1, hugeArray.Length - 1));

        Task.WaitAll(task1, task2, task3);            

        double sum = task1.Result + task2.Result + task3.Result;
    }




    public static void TaskWithReturnValue()
    {
        var sw = new Stopwatch();
            
        Console.WriteLine("Generating");
        int[] hugeArray = getHugeArray();
            
        Console.WriteLine("Calculating");
        sw.Start();
        var task1 = new Task<double>(() => calculate(hugeArray, 0, hugeArray.Length / 3));
        var task2 = new Task<double>(() => calculate(hugeArray, hugeArray.Length / 3 + 1, 2 * hugeArray.Length / 3));
        var task3 = new Task<double>(() => calculate(hugeArray, 2* hugeArray.Length / 3 + 1, hugeArray.Length -1));
            
        task1.Start();
        task2.Start();
        task3.Start();
            
        Task.WaitAll(task1, task2, task3);

        var sum = task1.Result + task2.Result + task3.Result;
        sw.Stop();
        Console.WriteLine($"Elapsed: {sw.Elapsed}");

        sw.Reset();
        sw.Start();
        calculate(hugeArray, 0, hugeArray.Length - 1);

        sw.Stop();
        Console.WriteLine($"Elapsed: {sw.Elapsed}");
    }

    private static double calculate(int[] array, int min, int max)
    {
        double sum = 0;
        for(int i = min; i <= max; i++)
        {
            sum += Math.Pow(array[i], -0.4) * Math.Pow(Math.Tanh(array[i]), 12);
        }

        return sum;
    }

    private static int[] getHugeArray()
    {
        const int arraySize = 100000000;
        var array = new int[arraySize];
        var randomGenerator = new Random();
        for(int i = 0; i < arraySize; i++)
        {
            array[i] = randomGenerator.Next();
        }

        return array;
    }
}