﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace CSharp2.Lesson01
{
    class Program
    {
        static int count = 0;
        static async Task Main(string[] args)
        {            
            var sw = new Stopwatch();
            sw.Start();
            


            sw.Stop();
            Console.WriteLine($"Elapsed: {sw.Elapsed}");
            
            Console.Read();
        }
    }
}
