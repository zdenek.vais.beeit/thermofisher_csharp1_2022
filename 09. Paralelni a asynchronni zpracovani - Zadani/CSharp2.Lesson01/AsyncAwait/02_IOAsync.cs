﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace BeeIT.Parallel1.AsyncAwait
{
    public static class IoAsyncExample
    {
        private const string URL = "http://www.seznamzpravy.cz";

        public static async Task RunAsync()
        {
            var result = await GetTextAsync();
            var arr = result.Split(new char[] { ' ', '.', ',', '!', '?' });            
            
            var count = Array.FindAll(arr, s => s.Contains("Babiš")).Length;


            Console.WriteLine($"{URL}: {count}");
        }

        private static async Task<string> GetTextAsync()
        {
            using (var httpClient = new HttpClient())
            {                
                return await httpClient.GetStringAsync(URL);                 
            }
        }

    }
}
