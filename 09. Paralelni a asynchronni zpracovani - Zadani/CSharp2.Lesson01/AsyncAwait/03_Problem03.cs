﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace BeeIT.Parallel1.AsyncAwait;

public static class AsyncProblem03
{
    private readonly static string[] Urls = new[] {
        "https://www.seznamzpravy.cz",
        "https://www.idnes.cz",
        "https://www.novinky.cz",
        "https://www.echo24.cz",
        "https://www.lidovky.cz",
    };


    public static async Task RunAsync()
    {
        
    }


}