﻿using System;
using System.Threading.Tasks;

namespace BeeIT.Parallel1.AsyncAwait;

public static class AsyncSyntax
{
    public static async Task<int> CalculationAsync()
    {

        try
        {
            await Task.Run(() =>
            {
                throw new ArgumentException();
            });
        }
        catch (ArgumentException argumentException)
        {

        }

        var result = await MethodAsync();
        return result;
    }

    public static async Task<int> MethodAsync()
    {
        await Task.Delay(1000);
        return 100;
    }
}