﻿using DungeonCrawler.Domain;
using System;

namespace DungeonCrawler.Terminal.Presentation
{
    public class ConsoleNotificationSubscriber : INotificationSubscriber
    {
        public void StateChange(string informationAboutChange)
        {
            Console.WriteLine(informationAboutChange);
        }
    }
}
