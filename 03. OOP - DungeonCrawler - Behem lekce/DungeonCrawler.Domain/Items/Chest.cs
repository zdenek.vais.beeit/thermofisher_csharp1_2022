﻿using System.Collections.Generic;

namespace DungeonCrawler.Domain.Items
{
    public class Chest : Item
    {
        private string keyIdentifier;

        private List<Item> items;

        private INotificationSubscriber changeNotifier;

        public bool Locked { get; private set; }

        public Chest(string name, string description, string keyIdentifier, 
                     IEnumerable<Item> items, INotificationSubscriber changeNotifier) 
            : base(name, description)
        {
            this.keyIdentifier = keyIdentifier;
            this.items = new List<Item>(items);
            this.changeNotifier = changeNotifier;
        }

        public override void UseOnMe(Item item)
        {
            Key key = item as Key;

            if (key != null && key.Identifier == keyIdentifier)
            {
                Locked = false;
                changeNotifier.StateChange("Truhla se otevřela");

            }
        }
    }
}
