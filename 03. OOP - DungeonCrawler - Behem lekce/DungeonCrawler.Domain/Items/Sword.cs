﻿namespace DungeonCrawler.Domain.Items
{
    public class Sword : Item
    {
        public int Damage { get; private set; }
        public Sword(string name, string description) : base(name, description)
        {             
        }   
    }
}
