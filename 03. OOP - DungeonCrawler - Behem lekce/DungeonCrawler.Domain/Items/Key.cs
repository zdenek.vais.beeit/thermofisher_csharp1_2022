﻿namespace DungeonCrawler.Domain.Items
{
    public class Key : Item
    {
        public string Identifier { get; private set; }

        public Key(string name, string description, string identifier) : base(name, description)
        {
            this.Identifier = identifier;
        }
    }
}
