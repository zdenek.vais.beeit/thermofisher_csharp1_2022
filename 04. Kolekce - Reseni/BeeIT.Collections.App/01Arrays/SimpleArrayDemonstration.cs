﻿namespace BeeIT.Collections.App._01Arrays;

internal class SimpleArrayDemonstration
{
    public static void Show()
    {
        // deklarace
        int[] poleCisel;
        string[] poleStringu;


        // inicializace, pocet prvku
        poleCisel = new int[5];                     // [15, 6, 87, 92, 114]
        poleStringu = new string[3];                // ["ab", "cd", "ef"]


        // indexy pole zacinaji v C# nulou [0, 1, 2, 3, 4, 5, ... ]
        poleCisel[4] = 987654;                      // [0, 0, 0, 0, 987654]
        poleStringu[1] = "asdf";                    // [null, "asdf", null]


        // vytazeni hodnoty pomoci indexu
        int cislo = poleCisel[2];                   // cislo == 0
        string text = poleStringu[1];               // text == "asdf"

        // pozor na vkladani/vyber do/z pole mimo indexu (vyhodi chybu) : poleCisel[8] = 5;

        // inicializace pomoci konstrukotru
        int[] pole = { 1, 3, 5, 7, 9 };
        string[] dniVTydnu = { "Pon", "Ute", "Str", "Ctv", "Pat", "Sob", "Ned" };


        // inicializace pomoci cyklu
        int[] pole3 = new int[10];

        for (int i = 0; i < 10; i++)
        {
            pole3[i] = i + 1;
        }

        // Delka pole
        int pocetPrvkuVPoli = pole.Length;

        // string je taky pole znaku
        text = "nejaky text";

        // vrati nam prvni znak
        char prvniZnak = text[0];                   // prvniZnak == 'n'

    }
}
