﻿namespace BeeIT.Collections.App._01Arrays;

    internal class MultidimensionalArrayDemonstration
    {
        public static void Show()
        {
            // dvourozmerne pole si muzeme predstavit jako tabulku nebo matici
            bool[,] kinosala = new bool[10, 12];            // vytvorime si tabulku mist s hodnotami bool (obsazeno - neobsazeno)

            // pristup k prvkum podobne jako pri jednorozmernych polich
            kinosala[0, 8] = true;                          // sedadlo cislo 9 v prvni rade je obsazeno
            bool jeSedadloObsazeno = kinosala[2, 6];        // je sedadlo c. 7 v treti rade obsazeno == default hodnota a teda false

            // podobne muzeme vytvaret N-rozmerna pole
            bool[,,] kinosaly = new bool[3, 10, 12];        // 3 saly s 10 radami a 12 sedadly v kazde rade
            jeSedadloObsazeno = kinosaly[1, 4, 3];          // druhy sal, pata rada, sedadlo c. 4

            // Length nam vrati celkovy pocet prvku v celem n-rozmernem poli
            int pocetPrvku = kinosala.Length;               // pocet prvku == 120

            // GetLength vraci pocet prvku v dane dimenzi (vhodne pro prochazeni matic v cyklu)
            // dimenze jsou stejne jako indexy pocitany od 0
            int pocetRad = kinosala.GetLength(0);           // pocet rad == 10
            int pocetSedadelvRade = kinosala.GetLength(1);  // pocet sedadel v rade == 12
        }
    }

