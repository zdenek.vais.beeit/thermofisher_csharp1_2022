﻿namespace BeeIT.Collections.App._07Problem;

internal class IsPalindromExample
{
    // abb -> bab -> true
    // aabb -> abba -> true
    // aabbc -> abcba -> true

    public static bool CanBePalindrom(string input)
    {
        if (string.IsNullOrWhiteSpace(input)) return false;

        var oddCharacters = new HashSet<char>();

        foreach (var character in input)
        {
            if (oddCharacters.Contains(character))
            {
                oddCharacters.Remove(character);
            }
            else
            {
                oddCharacters.Add(character);
            }
        }
       
        return oddCharacters.Count <= 1;
    }
}

