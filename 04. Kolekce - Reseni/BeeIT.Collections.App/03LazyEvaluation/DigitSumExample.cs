﻿namespace BeeIT.Collections.App._03LazyEvaluation;

internal class DigitSumExample
{
    public static int DigitSum(int input)
    {
        int sum = 0, reminder = 0;
        while (input > 0)
        {
            input = Math.DivRem(input, 10, out reminder);
            sum += reminder;
        }

        return sum;
    }

    public static int DigitSum2(int input)
    {
        int sum = 0, reminder = 0;
        while (input > 0)
        {
            reminder = input % 10;
            sum += reminder;
            input /= 10;
        }

        return sum;
    }

    public static int DigitSum3(int input)
    {
        int sum = 0;
        foreach (var numberChar in input.ToString())
        {
            sum += Convert.ToInt32(numberChar);
        }

        return sum;
    }

    public static int TotalDigitSum(int input)
    {
        if (input / 10 == 0) return input; 

        int sum = 0;
        while (input > 0)
        {
            input = Math.DivRem(input, 10, out int reminder);
            sum += reminder;
        }

        return TotalDigitSum(sum);
    }







}

