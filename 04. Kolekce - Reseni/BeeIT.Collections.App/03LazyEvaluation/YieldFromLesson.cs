﻿namespace BeeIT.Collections.App._03LazyEvaluation
{
    internal class YieldFromLesson
    {
        public static void FindTotalDigitSum()
        {
            int foundCount = 0;
            foreach (var number in NumberGenerator.GetNaturalNumbers())
            {
                if (DigitSumExample.TotalDigitSum(number) == 5)
                {
                    Console.WriteLine(number);
                    foundCount++;
                }

                if (foundCount >= 5)
                {
                    break;
                }
            }
        }

    }
}
