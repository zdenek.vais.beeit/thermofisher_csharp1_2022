﻿namespace BeeIT.Collections.App._03LazyEvaluation;

public class YieldExample
{
    public static IEnumerable<string> ZiskejZamestnance()
    {
        yield return "Karel Novak";
        yield return "Tereza Bohatá";
        yield return "Kamila Chudá";
        yield return "Zdeněk Rychlý";
        yield return "Klára Pomalá";
        yield return "Josef Pohoda";
        yield return "Eva Hrozná";
        yield return "Kvído Kopecký";
    }

    public static void VypisPrvni3ZamestnanceSJmenemNaK()
    {
        int nalezenych = 0;
        var zamestnanci = ZiskejZamestnance();
        foreach (var jmeno in zamestnanci)
        {
            if (jmeno.StartsWith("K"))
            {
                Console.WriteLine(jmeno);
                nalezenych++;
            }

            if (nalezenych >= 2)
            {
                break;
            }
        }
    }
}
