﻿namespace BeeIT.Collections.App._02Foreach
{
    internal class ForeachProblem
    {
        public static void Example()
        {
            var seznam = new List<string> { "A", "B", "C" };
            foreach (var prvek in seznam)
            {
                if (prvek == "A")
                {
                    seznam.Remove(prvek); // Spadne
                }
            }

        }
    }
}
