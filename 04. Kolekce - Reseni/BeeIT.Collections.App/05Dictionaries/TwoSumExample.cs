﻿namespace BeeIT.Collections.App._05Dictionaries;

internal class TwoSumExample
{
    public IEnumerable<(int, int)> TwoSum(int[] array, int sum)
    {
        var dictionary = new Dictionary<int, int>();

        for (int i = 0; i < array.Length; i++)
        {
            int current = array[i];
            int complement = sum - current;
           
            if (dictionary.TryGetValue(complement, out int previousIndex))
            {
                yield return (i, previousIndex);
            }
            else
            {
                dictionary.Add(current, i);
            }
        }
    }
}

