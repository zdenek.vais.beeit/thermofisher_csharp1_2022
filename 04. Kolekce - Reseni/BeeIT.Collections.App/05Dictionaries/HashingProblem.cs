﻿namespace BeeIT.Collections.App._05Dictionaries;

public class User
{
    public string Name { get; set; }
    public int Age { get; set; }
    public override int GetHashCode()
    {
        return Name.GetHashCode() ^ Age.GetHashCode();
    }

    public override bool Equals(object? obj)
    {
        if (obj == null) return false;

        if (obj is User user)
        {
            return this.Name == user.Name && this.Age == user.Age;
        }

        return false;
    }
}
internal class HashingProblem
{
    public static void Show()
    {
        var user1 = new User
        {
            Age = 18,
            Name = "Zdenek"
        };

        var user2 = new User
        {
            Age = 18,
            Name = "Zdenek"
        };

        var dictionary = new Dictionary<User, string>();
        dictionary.Add(user1, "Motto");

        var result = dictionary.ContainsKey(user2);
       

        Console.WriteLine(dictionary.Count);
    }
}

