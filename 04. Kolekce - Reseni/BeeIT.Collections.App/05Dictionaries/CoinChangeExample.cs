﻿namespace BeeIT.Collections.App._05Dictionaries;

internal class CoinChangeExample
{
    private static readonly int[] nominalValues = { 50, 20, 10, 5, 2, 1 };

    // Vratte nejmensi pocet minci, kterymi je mozne vratit zadanou castku.
    public static Dictionary<int, int> Change(int amount)
    {
        var result = new Dictionary<int, int>();

        foreach (var nominalValue in nominalValues)
        {
            int coinCount = amount / nominalValue;
            amount -= coinCount * nominalValue; //  amount %= nominalValue;

            // Dalsi moznosti by bylo pouzit funkci ze tridy Math:
            // int coinCount = Math.DivRem(amount, nominalValue, out amount);

            result[nominalValue] = coinCount;
        }

        return result;
    }
}

