﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Linq.Comparer
{
	internal static class ObceExtensions
	{
		public static int SoucetObyvatel(this IEnumerable<Obec> obce)
		{
			return obce.Sum(x => x.PocetObyvatel);
		}
	}
}
