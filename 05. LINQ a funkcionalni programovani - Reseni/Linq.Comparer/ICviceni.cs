﻿using System.Linq;
using System.Collections.Generic;

namespace Linq.Comparer
{
	public interface ICviceni
	{
		int? PocetObci(IEnumerable<Obec> obce);
		int? SoucetObyvatel(IEnumerable<int> pocetObyvatelObci);
		double? PrumerObyvatelNaObec(IEnumerable<int> pocetObyvatelObci);
		int? NejvetsiPocetObyvatelVJedneObci(IEnumerable<int> pocetObyvatelObci);
		int? NejmensiPocetObyvatelVJedneObci(IEnumerable<int> pocetObyvatelObci);
		IEnumerable<Obec> ObceFiltr1(IEnumerable<Obec> obce);
		IEnumerable<Obec> ObceFiltr2(IEnumerable<Obec> obce);
		IEnumerable<Obec> ObceFiltr3(IEnumerable<Obec> obce);
		IEnumerable<Obec> ObceFiltr4(IEnumerable<Obec> obce);
		IEnumerable<Obec> ObceFiltr5(IEnumerable<Obec> obce);
		IEnumerable<Obec> ObceFiltr6(IEnumerable<Obec> obce);
		IEnumerable<Obec> ObceFiltr7(IEnumerable<Obec> obce);
		IEnumerable<Obec> ObceFiltr8(IEnumerable<Obec> obce);
		IEnumerable<Obec> Serazeni1(IEnumerable<Obec> obce);
		IEnumerable<Obec> Serazeni2(IEnumerable<Obec> obce);
		IEnumerable<string> SelectNazev(IEnumerable<Obec> obce);
		IEnumerable<int> SelectPocetObyvatel(IEnumerable<Obec> obce);
		IEnumerable<string> SelectZakladniPusobnost(IEnumerable<Obec> obce);
		bool? JakykolivObsajujeX(IEnumerable<Obec> obce);
		bool? VsichniObsahujiA(IEnumerable<Obec> obce);
		Obec Prvni(IEnumerable<Obec> obce);
		Obec PrvniNeboZadny(IEnumerable<Obec> obce);
		int? PocetObciSObyvateli(IEnumerable<Obec> obce);
		int? PocetObciVPardubickemKraji(IEnumerable<Obec> obce);
		int? PocetObyvatelVZlínskémKraji(IEnumerable<Obec> obce);
		double? PrumernyPocetObyvatelVMoravskoslezskemKraji(IEnumerable<Obec> obce);
		IEnumerable<Obec> NejlidnatejsiObcevJihomoravskemKraji(IEnumerable<Obec> obce);
		int? PocetObciSPracovnimUrademVStredoceskemKraji(IEnumerable<Obec> obce);
		IEnumerable<Kraj> KrajeSViceNez1MlnObyvateli(IEnumerable<Obec> obce);
		Dictionary<Kraj, int> NejviceObyvatelVKazdemKraji(IEnumerable<Obec> obce);
	}
}
