﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Linq.Comparer;
using System.Diagnostics;

namespace Linq
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WindowWidth = 130;
            Console.WindowHeight = 40;
            Console.OutputEncoding = Encoding.UTF8;

            var loader = new DataLoader();
            var mesta = loader.NactiObce();

            var cviceniUloha = new CviceniUloha();
            var cviceniReseni = new CviceniReseni();
            var comparer = new DataComparer(cviceniUloha, cviceniReseni);
            comparer.Compare(mesta);

            Console.ReadKey();
        }
    }
}
