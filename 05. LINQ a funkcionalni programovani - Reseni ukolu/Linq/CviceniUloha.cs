﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Linq.Comparer;

namespace Linq
{
    public class CviceniUloha : ICviceni
    {
        // 1. Počet obcí
        public int? PocetObci(IEnumerable<Obec> obce)
        {
            return obce.Count();
        }

        // 2. Součet obyvatel všech obcí
        public int? SoucetObyvatel(IEnumerable<int> pocetObyvatelObci)
        {
            return pocetObyvatelObci.Sum();
        }

        // 3. Průměrný počet obyvatel na jednu obec
        public double? PrumerObyvatelNaObec(IEnumerable<int> pocetObyvatelObci)
        {
            return pocetObyvatelObci.Average();
        }

        // 4. Největší počet obyvatel v jedné obci
        public int? NejvetsiPocetObyvatelVJedneObci(IEnumerable<int> pocetObyvatelObci)
        {
            return pocetObyvatelObci.Max();
        }

        // 5. Nejmenší počet obyvatel v jedné obci
        public int? NejmensiPocetObyvatelVJedneObci(IEnumerable<int> pocetObyvatelObci)
        {
            return pocetObyvatelObci.Min();
        }

        // 6. Pouze seznamObci s více než 100000 obyvateli
        public IEnumerable<Obec> ObceFiltr1(IEnumerable<Obec> obce)
        {
            return obce.Where(x => x.PocetObyvatel > 100000);
        }

        // 7. Pouze seznamObci v Jihomoravském kraji
        public IEnumerable<Obec> ObceFiltr2(IEnumerable<Obec> obce)
        {
            return obce.Where(x => x.Kraj == Kraj.Jihomoravsky);
        }

        // 8. Pouze seznamObci, která mají CzechPoint
        public IEnumerable<Obec> ObceFiltr3(IEnumerable<Obec> obce)
        {
            return obce.Where(x => x.MaCzechPoint);
        }

        // 9. Pouze města s matričním úřadem
        public IEnumerable<Obec> ObceFiltr4(IEnumerable<Obec> obce)
        {
            return obce.Where(x => x.Kategorie == Kategorie.Mesto && x.MaMatricniUrad);
        }

        // 10. Pouze seznamObci, která mají regionální pracovniště a zároveň pracovní úřad
        public IEnumerable<Obec> ObceFiltr5(IEnumerable<Obec> obce)
        {
            return obce.Where(x => x.MaRegionalniPracoviste && x.MaPracovniUrad);
        }

        // 11. Pouze seznamObci v kategorii městys nebo vesnice
        public IEnumerable<Obec> ObceFiltr6(IEnumerable<Obec> obce)
        {
            return obce.Where(x => x.Kategorie == Kategorie.Mestys || x.Kategorie == Kategorie.Vesnice);
        }

        // 12. Pouze seznamObci se statusem město nebo statutární město
        public IEnumerable<Obec> ObceFiltr7(IEnumerable<Obec> obce)
        {
            return obce.Where(x => x.Kategorie == Kategorie.Mesto || x.Kategorie == Kategorie.StatutarniMesto);
        }

        // 13. Vesnice v Jihomoravském kraji
        public IEnumerable<Obec> ObceFiltr8(IEnumerable<Obec> obce)
        {
            return obce.Where(x => x.Kategorie == Kategorie.Vesnice && x.Kraj == Kraj.Jihomoravsky);
        }

        // 14. Seřazené seznamObci podle počtu obyvatel vzestupně
        public IEnumerable<Obec> Serazeni1(IEnumerable<Obec> obce)
        {
            return obce.OrderBy(x => x.PocetObyvatel);
        }

        // 15. Seřazené seznamObci podle počtu obyvatel sestupně
        public IEnumerable<Obec> Serazeni2(IEnumerable<Obec> obce)
        {
            return obce.OrderByDescending(x => x.PocetObyvatel);
        }

        // 16. Pouze názvy obcí
        public IEnumerable<string> SelectNazev(IEnumerable<Obec> obce)
        {
            return obce.Select(x => x.Nazev);
        }

        // 17. Pouze počty obyvatel
        public IEnumerable<int> SelectPocetObyvatel(IEnumerable<Obec> obce)
        {
            return obce.Select(x => x.PocetObyvatel);
        }

        // 18. Pouze hodnoty základní působnosti
        public IEnumerable<string> SelectZakladniPusobnost(IEnumerable<Obec> obce)
        {
            return obce.Select(x => x.ZakladniPusobnost);
        }

        // 19. Existuje obec, které má v názvu "x"?
        public bool? JakykolivObsajujeX(IEnumerable<Obec> obce)
        {
            return obce.Any(x => x.Nazev.Contains("x"));
        }

        // 20. Mají všechny seznamObci v názvu "a"?
        public bool? VsichniObsahujiA(IEnumerable<Obec> obce)
        {
            return obce.All(x => x.Nazev.Contains("a"));
        }

        // 21. První město v pořadí
        public Obec Prvni(IEnumerable<Obec> obce)
        {
            return obce.First(x => x.Kategorie == Kategorie.Mesto);
        }

        // 22. První nebo žádná obec
        public Obec PrvniNeboZadny(IEnumerable<Obec> obce)
        {
            return obce.FirstOrDefault();
        }

        // 23. Počet obcí s více než 10000 obyvateli
        public int? PocetObciSObyvateli(IEnumerable<Obec> obce)
        {
            return obce.Count(x => x.PocetObyvatel > 10000);
        }

        // 24. Počet obcí v Pardubickém kraji
        public int? PocetObciVPardubickemKraji(IEnumerable<Obec> obce)
        {
            return obce.Count(x => x.Kraj == Kraj.Pardubicky);
        }

        //==============================================================

        // 25. Počet obyvatel v Zlínském kraji
        public int? PocetObyvatelVZlínskémKraji(IEnumerable<Obec> seznamObci)
        {
            int pocetObyvatel = 0;
            foreach (var obec in seznamObci)
            {
                if (obec.Kraj == Kraj.Zlinsky)
                {
                    pocetObyvatel += obec.PocetObyvatel;
                }
            }

            return pocetObyvatel;
        }

        // 26. Průměrný počet obyvatel v Moravskoslezském kraji na jednu obec
        public double? PrumernyPocetObyvatelVMoravskoslezskemKraji(IEnumerable<Obec> seznamObci)
        {
            int pocetObyvatel = 0;
            int pocetObci = 0;
            foreach (var obec in seznamObci)
            {
                if (obec.Kraj == Kraj.Moravskoslezsky)
                {
                    pocetObyvatel += obec.PocetObyvatel;
                    pocetObci += 1;
                }
            }

            if (pocetObci == 0) return 0;

            return (double)pocetObyvatel / pocetObci;
        }

        // 27. Prvních 5 nejlidnatnějších měst v Jihomoravském kraji
        public IEnumerable<Obec> NejlidnatejsiObcevJihomoravskemKraji(IEnumerable<Obec> obce)
        {
            var seznamObci = obce.ToArray();
            var nejlidnatejsiObce = new SortedSet<Obec>(Comparer<Obec>.Create((x, y) => x.PocetObyvatel - y.PocetObyvatel))
                { seznamObci[0], seznamObci[1], seznamObci[2], seznamObci[3], seznamObci[4] };

            for (int i = 5; i < seznamObci.Length; i++)
            {
                var soucasneMesto = seznamObci[i];

                if (soucasneMesto.Kraj != Kraj.Jihomoravsky || soucasneMesto.Kategorie != Kategorie.Mesto)
                {
                    continue;
                }

                if (soucasneMesto.PocetObyvatel > nejlidnatejsiObce.Min.PocetObyvatel)
                {
                    nejlidnatejsiObce.Remove(nejlidnatejsiObce.Min);
                    nejlidnatejsiObce.Add(soucasneMesto);
                }

            }
            return nejlidnatejsiObce;

        }

        // 28. Počet obcí s pracovním úradem v Středočeském kraji
        public int? PocetObciSPracovnimUrademVStredoceskemKraji(IEnumerable<Obec> seznamObci)
        {
            int count = 0;

            foreach (var obec in seznamObci)
            {
                if (obec.Kraj == Kraj.Stredocesky && obec.MaPracovniUrad)
                {
                    count++;
                }
            }

            return count;
        }

        //==============================================================

        // 29. Kraje, které mají více než 1 mln obyvatel
        public IEnumerable<Kraj> KrajeSViceNez1MlnObyvateli(IEnumerable<Obec> seznamObci)
        {
            var pocetObyvatelPodleKraje = new Dictionary<Kraj, int>();
            foreach (var obec in seznamObci)
            {
                pocetObyvatelPodleKraje.TryGetValue(obec.Kraj, out int pocetObyvatel);
                pocetObyvatel += obec.PocetObyvatel;

                pocetObyvatelPodleKraje[obec.Kraj] = pocetObyvatel;
            }

            foreach (var kraj in pocetObyvatelPodleKraje)
            {
                if (kraj.Value > 1_000_000)
                {
                    yield return kraj.Key;
                }
            }
        }

        // 30. Nejvíce obyvatel v každém kraji
        public Dictionary<Kraj, int> NejviceObyvatelVKazdemKraji(IEnumerable<Obec> seznamObci)
        {
            var maximalniPocetObyvateluDleKraje = new Dictionary<Kraj, int>();
            foreach (var obec in seznamObci)
            {
                maximalniPocetObyvateluDleKraje.TryGetValue(obec.Kraj, out int pocetObyvatel);
                
                if (obec.PocetObyvatel > pocetObyvatel)
                {
                    maximalniPocetObyvateluDleKraje[obec.Kraj] = obec.PocetObyvatel;
                }
            }

            return maximalniPocetObyvateluDleKraje;
        }
    }
}
