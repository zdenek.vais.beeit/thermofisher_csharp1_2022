﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq.Comparer
{
	public enum Kraj
	{
		Karlovarsky,
		Jihocesky,
		Stredocesky,
		Jihomoravsky,
		Kralovehradecky,
		Moravskoslezsky,
		Pardubicky,
		Liberecky,
		Olomoucky,
		Vysocina,
		Ustecky,
		Zlinsky,
		Plzensky,
		Praha,
	}
}
