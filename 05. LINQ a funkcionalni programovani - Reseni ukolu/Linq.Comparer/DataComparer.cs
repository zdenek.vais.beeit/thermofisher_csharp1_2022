﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq.Comparer
{
	public class DataComparer
	{
		private readonly ICviceni _cviceniUloha;
		private readonly ICviceni _cviceniReseni;

		public DataComparer(ICviceni cviceniUloha, ICviceni cviceniReseni)
		{
			_cviceniUloha = cviceniUloha;
			_cviceniReseni = cviceniReseni;
		}

		public void Compare(IEnumerable<Obec> obce)
		{
			Console.WriteLine("Porovnavam vysledky...");

			//CompareResults2(1, obce, _cviceniUloha.PocetObci, _cviceniReseni.PocetObci);
			//CompareResults2(2, obce.Select(x => x.PocetObyvatel), _cviceniUloha.SoucetObyvatel, _cviceniReseni.SoucetObyvatel);
			//CompareResults2(3, obce.Select(x => x.PocetObyvatel), _cviceniUloha.PrumerObyvatelNaObec, _cviceniReseni.PrumerObyvatelNaObec);
			//CompareResults2(4, obce.Select(x => x.PocetObyvatel), _cviceniUloha.NejvetsiPocetObyvatelVJedneObci, _cviceniReseni.NejvetsiPocetObyvatelVJedneObci);
			//CompareResults2(5, obce.Select(x => x.PocetObyvatel), _cviceniUloha.NejmensiPocetObyvatelVJedneObci, _cviceniReseni.NejmensiPocetObyvatelVJedneObci);
			//CompareResults1(6, obce, _cviceniUloha.ObceFiltr1, _cviceniReseni.ObceFiltr1);
			//CompareResults1(7, obce, _cviceniUloha.ObceFiltr2, _cviceniReseni.ObceFiltr2);
			//CompareResults1(8, obce, _cviceniUloha.ObceFiltr3, _cviceniReseni.ObceFiltr3);
			//CompareResults1(9, obce, _cviceniUloha.ObceFiltr4, _cviceniReseni.ObceFiltr4);
			//CompareResults1(10, obce, _cviceniUloha.ObceFiltr5, _cviceniReseni.ObceFiltr5);
			//CompareResults1(11, obce, _cviceniUloha.ObceFiltr6, _cviceniReseni.ObceFiltr6);
			//CompareResults1(12, obce, _cviceniUloha.ObceFiltr7, _cviceniReseni.ObceFiltr7);
			//CompareResults1(13, obce, _cviceniUloha.ObceFiltr8, _cviceniReseni.ObceFiltr8);
			//CompareResults1(14, obce, _cviceniUloha.Serazeni1, _cviceniReseni.Serazeni1, true);
			//CompareResults1(15, obce, _cviceniUloha.Serazeni2, _cviceniReseni.Serazeni2, true);
			//CompareResults1(16, obce, _cviceniUloha.SelectNazev, _cviceniReseni.SelectNazev);
			//CompareResults1(17, obce, _cviceniUloha.SelectPocetObyvatel, _cviceniReseni.SelectPocetObyvatel);
			//CompareResults1(18, obce, _cviceniUloha.SelectZakladniPusobnost, _cviceniReseni.SelectZakladniPusobnost);
			//CompareResults2(19, obce, _cviceniUloha.JakykolivObsajujeX, _cviceniReseni.JakykolivObsajujeX);
			//CompareResults2(20, obce, _cviceniUloha.VsichniObsahujiA, _cviceniReseni.VsichniObsahujiA);
			//CompareResults2(21, obce, _cviceniUloha.Prvni, _cviceniReseni.Prvni);
			//CompareResults2(22, obce, _cviceniUloha.PrvniNeboZadny, _cviceniReseni.PrvniNeboZadny);
			//CompareResults2(23, obce, _cviceniUloha.PocetObciSObyvateli, _cviceniReseni.PocetObciSObyvateli);
			//CompareResults2(24, obce, _cviceniUloha.PocetObciVPardubickemKraji, _cviceniReseni.PocetObciVPardubickemKraji);
			CompareResults2(25, obce, _cviceniUloha.PocetObyvatelVZlínskémKraji, _cviceniReseni.PocetObyvatelVZlínskémKraji);
			CompareResults2(26, obce, _cviceniUloha.PrumernyPocetObyvatelVMoravskoslezskemKraji, _cviceniReseni.PrumernyPocetObyvatelVMoravskoslezskemKraji);
			CompareResults1(27, obce, _cviceniUloha.NejlidnatejsiObcevJihomoravskemKraji, _cviceniReseni.NejlidnatejsiObcevJihomoravskemKraji);
			CompareResults2(28, obce, _cviceniUloha.PocetObciSPracovnimUrademVStredoceskemKraji, _cviceniReseni.PocetObciSPracovnimUrademVStredoceskemKraji);
			CompareResults1(29, obce, _cviceniUloha.KrajeSViceNez1MlnObyvateli, _cviceniReseni.KrajeSViceNez1MlnObyvateli);
			CompareResults3(30, obce, _cviceniUloha.NejviceObyvatelVKazdemKraji, _cviceniReseni.NejviceObyvatelVKazdemKraji);
		}

		private void CompareResults1<TIn, TOut>(int index, IEnumerable<TIn> Obcia,
			Func<IEnumerable<TIn>, IEnumerable<TOut>> ulohaFunc, Func<IEnumerable<TIn>, IEnumerable<TOut>> reseniFunc,
			bool compareCountsOnly = false)
		{
			Console.Write($"Uloha {index,2}:\t");

			var ulohaVysledek = GetResult(ulohaFunc, Obcia);
			if (ulohaVysledek == null)
			{
				return;
			}
			var ulohaReseni = reseniFunc(Obcia);

			if (compareCountsOnly)
			{
				if (ulohaVysledek.Count() != ulohaReseni.Count())
				{
					Console.ForegroundColor = ConsoleColor.Red;
					Console.WriteLine($"Chyba - jiny pocet");
					Console.ForegroundColor = ConsoleColor.Gray;
					return;
				}
			}
			else
			{
				var addedItem = ulohaVysledek.FirstOrDefault(x => !ulohaReseni.Contains(x));
				if (!EqualityComparer<TOut>.Default.Equals(addedItem, default))
				{
					Console.ForegroundColor = ConsoleColor.Red;
					Console.WriteLine($"Chyba - nadbyva: {addedItem}");
					Console.ForegroundColor = ConsoleColor.Gray;
					return;
				}

				var removedItem = ulohaReseni.FirstOrDefault(x => !ulohaVysledek.Contains(x));
				if (!EqualityComparer<TOut>.Default.Equals(addedItem, default))
				{
					Console.ForegroundColor = ConsoleColor.Red;
					Console.WriteLine($"Chyba - chybi: {removedItem}");
					Console.ForegroundColor = ConsoleColor.Gray;
					return;
				}
			}

			Console.ForegroundColor = ConsoleColor.Green;
			Console.WriteLine("Ok");
			Console.ForegroundColor = ConsoleColor.Gray;
		}

		private void CompareResults2<TIn, TOut>(int index, IEnumerable<TIn> obce,
			Func<IEnumerable<TIn>, TOut> ulohaFunc, Func<IEnumerable<TIn>, TOut> reseniFunc)
		{
			Console.Write($"Uloha {index,2}:\t");

			var ulohaVysledek = GetResult(ulohaFunc, obce);
			if (ulohaVysledek == null)
			{
				return;
			}
			var ulohaReseni = reseniFunc(obce);

			if (!EqualityComparer<TOut>.Default.Equals(ulohaReseni, ulohaVysledek))
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine($"Chyba - jiny vysledek: {ulohaReseni} vs {ulohaVysledek}");
				Console.ForegroundColor = ConsoleColor.Gray;
				return;
			}

			Console.ForegroundColor = ConsoleColor.Green;
			Console.WriteLine("Ok");
			Console.ForegroundColor = ConsoleColor.Gray;
		}

		private void CompareResults3<TIn, TOut1, TOut2>(int index, IEnumerable<TIn> Obcia,
			Func<IEnumerable<TIn>, Dictionary<TOut1, TOut2>> ulohaFunc, Func<IEnumerable<TIn>, Dictionary<TOut1, TOut2>> reseniFunc)
		{
			Console.Write($"Uloha {index,2}:\t");

			var ulohaVysledek = GetResult(ulohaFunc, Obcia);
			if (ulohaVysledek == null)
			{
				return;
			}
			var ulohaReseni = reseniFunc(Obcia);

			var addedItem = ulohaVysledek.FirstOrDefault(x => !ulohaReseni.Contains(x));
			if (!EqualityComparer<KeyValuePair<TOut1, TOut2>>.Default.Equals(addedItem, default))
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine($"Chyba - nadbyva: {addedItem}");
				Console.ForegroundColor = ConsoleColor.Gray;
				return;
			}

			var removedItem = ulohaReseni.FirstOrDefault(x => !ulohaVysledek.Contains(x));
			if (!EqualityComparer<KeyValuePair<TOut1, TOut2>>.Default.Equals(addedItem, default))
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine($"Chyba - chybi: {removedItem}");
				Console.ForegroundColor = ConsoleColor.Gray;
				return;
			}

			Console.ForegroundColor = ConsoleColor.Green;
			Console.WriteLine("Ok");
			Console.ForegroundColor = ConsoleColor.Gray;
		}

		private TOut GetResult<TIn, TOut>(Func<IEnumerable<TIn>, TOut> func, IEnumerable<TIn> input)
		{
			TOut ulohaVysledek;
			try
			{
				ulohaVysledek = func(input);
			}
			catch (Exception)
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine("Chyba: Vyjimka");
				Console.ForegroundColor = ConsoleColor.Gray;
				return default;
			}

			if (ulohaVysledek == null)
			{
				Console.ForegroundColor = ConsoleColor.Yellow;
				Console.WriteLine("Neimplementovano");
				Console.ForegroundColor = ConsoleColor.Gray;
				return default;
			}

			return ulohaVysledek;
		}
	}
}
