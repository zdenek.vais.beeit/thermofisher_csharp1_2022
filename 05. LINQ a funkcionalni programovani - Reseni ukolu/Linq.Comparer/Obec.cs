﻿using System.Diagnostics;

namespace Linq.Comparer
{
	[DebuggerDisplay("{Nazev}")]
	public class Obec
	{
		public int Index { get; set; }
		public string Nazev { get; set; }
		public int PocetObyvatel { get; set; }
		public int Id { get; set; }
		public int IdentifikacniCisloOrganizace { get; set; }
		public Kategorie Kategorie { get; set; }
		public string Okres { get; set; }
		public int OkresId { get; set; }
		public Kraj Kraj { get; set; }
		public string ZakladniPusobnost { get; set; }
		public bool MaMatricniUrad { get; set; }
		public bool MaPracovniUrad { get; set; }
		public bool MaRegionalniPracoviste { get; set; }
		public bool MaVidimacniUrad { get; set; }
		public bool MaCzechPoint { get; set; }

		public override string ToString()
		{
			return $"{Index,2}: {Nazev}, {PocetObyvatel}, {Kategorie}, {Okres}, {Kraj}, " +
					$"ZP:{ZakladniPusobnost}, MU:{PrintBool(MaMatricniUrad)}, PU:{PrintBool(MaPracovniUrad)}, " +
					$"RP:{PrintBool(MaRegionalniPracoviste)}, VU:{PrintBool(MaVidimacniUrad)}, CZ:{PrintBool(MaCzechPoint)}";
		}

        public override bool Equals(object obj)
        {
			if(obj is Obec obec)
            {
                return obec.Id == Id;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        private string PrintBool(bool value)
		{
			return value ? "1" : "0";
		}
	}
}
