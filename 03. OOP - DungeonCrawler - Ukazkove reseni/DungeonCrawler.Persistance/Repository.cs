﻿using System;
using System.IO;
using System.Text.Json;

namespace DungeonCrawler.Persistance
{
    public class Repository<T>
    {
        public T Get(string filePath)
        {
            var fileContent = File.ReadAllText(filePath);
            return JsonSerializer.Deserialize<T>(fileContent);
        }

        public void Save(string filePath, T obj)
        {
            var json = JsonSerializer.Serialize(obj);
            File.WriteAllText(filePath , json);
        }
    }
}
