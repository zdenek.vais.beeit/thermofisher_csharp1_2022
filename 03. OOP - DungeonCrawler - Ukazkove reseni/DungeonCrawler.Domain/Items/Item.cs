﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonCrawler.Domain
{
    public abstract class Item
    {
        public string Name { get; private set; }
        public string Description { get; private set; }

        public Item(string name, string description)
        {
            this.Name = name;
            this.Description = description;
        }

        public virtual void UseOnMe(Item item)
        {    
        }
    }
}
