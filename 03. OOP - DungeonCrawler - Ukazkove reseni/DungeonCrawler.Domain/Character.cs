﻿using DungeonCrawler.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace DungeonCrawler
{
    public class Character
    {
        private int maxHealth;

        public string Name { get; private set; }

        public int Health { get; private set; }

        public int Xp { get; private set; }

        public List<Item> Inventory { get; set; }

        public Character(string name, int maxHealth, IEnumerable<Item> items)
        {
            this.Name = name;
            this.Health = maxHealth;
            this.maxHealth = maxHealth;
            this.Inventory = new List<Item>(items);
        }

        public override string ToString()
        {
            return $"Name: {Name}, Health: {Health}";
        }
    }
}
