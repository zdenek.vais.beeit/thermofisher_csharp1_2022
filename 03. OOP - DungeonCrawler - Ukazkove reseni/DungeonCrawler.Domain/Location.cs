﻿using System.Collections.Generic;

namespace DungeonCrawler.Domain
{
    public class Location
    {
        public string Description { get; set; }
        public List<Item> Items { get; set; }

        public Location(string description, IEnumerable<Item> items)
        {
            this.Description = description;
            this.Items = new List<Item>(items);
        }
    }
}
