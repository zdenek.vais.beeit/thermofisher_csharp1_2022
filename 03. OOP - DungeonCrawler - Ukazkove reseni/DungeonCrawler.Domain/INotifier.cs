﻿namespace DungeonCrawler.Domain
{
    /// <summary>
    /// Slouzi k informovani o zmene stavu predmetu
    /// </summary>
    public interface INotificationSubscriber
    {
        void StateChange(string informationAboutChange);
    }
}
