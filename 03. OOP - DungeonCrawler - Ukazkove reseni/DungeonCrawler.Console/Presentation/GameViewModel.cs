﻿using System;
using System.Collections.Generic;
using System.Linq;
using DungeonCrawler.Domain;

namespace DungeonCrawler.Terminal.Presentation
{
    internal class GameViewModel
    {
        private Character character;
        private Location location;
        public GameViewModel(Character character, Location location)
        {
            this.character = character;
            this.location = location;
        }

        public void ShowLocation()
        {
            Console.WriteLine(location.Description);
            Console.WriteLine("Předměty v místnosti:");
            writeOutItems(location.Items);
        }
        public void ShowDetails()
        {
            Console.WriteLine(character.Name);
            Console.WriteLine($"  HP: {character.Health}");
            Console.WriteLine($"  XP: {character.Xp}");
        }

        public void ShowInventory()
        {
            Console.WriteLine("Inventář:");
            if (character.Inventory.Any())
            {
                writeOutItems(character.Inventory);
            }
        }

        public void UseItem()
        {
            Console.WriteLine("Který předmět si přejete použít?:");
            writeOutItems(character.Inventory);

            // Vyber predmetu
            Item itemToUse = selectItem(character.Inventory);         

            Console.WriteLine($"Na který předmět v místnosti chcete použít {itemToUse.Name}?");
            writeOutItems(location.Items);
            Item itemToBeUsedOn = selectItem(location.Items);

            itemToBeUsedOn.UseOnMe(itemToUse);

        }

        private void writeOutItems(List<Item> items)
        {
            for (int i = 0; i < items.Count; i++)
            {
                var item = items[i];
                Console.WriteLine($"{i} - {item.Name}");
            }
        }


        private Item selectItem(List<Item> items)
        {
            while (true)
            {
                string inputForItem = Console.ReadLine();
                if (!int.TryParse(inputForItem, out int itemIndex))
                {
                    // Nejde naparsovat
                    Console.WriteLine("Nevalidni vstup");
                    continue;
                }

                if (itemIndex >= items.Count)
                {
                    Console.WriteLine("Takovy predmet neexistuje");
                    continue;
                }

                return items[itemIndex]; // Ukonci smycku                
            }
        }
    }
}
