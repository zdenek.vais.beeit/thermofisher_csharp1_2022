﻿using DungeonCrawler.Domain;
using DungeonCrawler.Domain.Items;
using DungeonCrawler.Terminal.Presentation;
using System;
using System.Linq;

namespace DungeonCrawler
{
    class Program
    {
        static void Main(string[] args)
        {
            // Slouzi k zobrazovani zmeny stavu apod. (bude vysvetleno na lekci)
            var consoleNotifier = new ConsoleNotificationSubscriber();

            var key = new Key("Velký měděný klíč", "", "klic1");
            var sword = new Sword("Starý rezavý meč", "");
            var chest1 = new Chest("Velká dubová truhla", 
                                   "Truhla vypadá, že ji nedávno někdo používal",
                                   "klic1",
                                   new[] { sword },
                                   consoleNotifier);

            var room1 = new Location("Nacházíte se ve staré místnosti plné pavouků.", new[] { chest1 });
            
            var player = new Character("Barban Conan", 100, new[] { key});
            var gameViewModel = new GameViewModel(player, room1);

            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("Zadejte příkaz:");
                string command = Console.ReadLine();
                switch (command.ToLower())
                {
                    case "h":
                    case "hrac":
                        gameViewModel.ShowDetails();
                        break;
                    case "i":
                    case "inventar":
                        gameViewModel.ShowInventory(); 
                        break;
                    case "p":
                    case "pouzij":
                        gameViewModel.UseItem();
                        break;
                    case "m":
                    case "misto": 
                        gameViewModel.ShowLocation();
                        break;                        
                    default:
                        Console.WriteLine("Neznámý příkaz command");
                        break;
                }

            }


        }
    }
}
