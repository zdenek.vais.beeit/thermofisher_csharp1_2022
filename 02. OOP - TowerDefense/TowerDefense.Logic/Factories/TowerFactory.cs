﻿using BeeIt.TowerDefense.Contracts.Interfaces;
using BeeIt.TowerDefense.Contracts.Math;
using BeeIt.TowerDefense.Logic.Towers;
using System.Collections.Generic;

namespace BeeIt.TowerDefense.Logic.Factories
{
	internal class TowerFactory : ITowerFactory
	{
		public IEnumerable<ITower> Create()
		{
			return new List<ITower>
			{
				new Tower(new Vector(6, 3), 1),
				new Tower(new Vector(1, 2), 1),
				new Tower(new Vector(6, 7), 1),
				new Tower(new Vector(4, 5), 1),
				new Tower(new Vector(2, 4), 1),
				new Tower(new Vector(4, 2), 1)
			};
		}
	}
}