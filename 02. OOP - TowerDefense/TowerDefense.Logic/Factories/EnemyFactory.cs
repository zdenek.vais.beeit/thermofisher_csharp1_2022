﻿using BeeIt.TowerDefense.Contracts.Interfaces;
using BeeIt.TowerDefense.Logic.Enemies;
using BeeIt.TowerDefense.Logic.Game;
using System.Collections.Generic;

namespace BeeIt.TowerDefense.Logic.Factories
{
	internal class EnemyFactory : IEnemyFactory
	{
		private int _level;
		private Path _path;

		public EnemyFactory(
			Path path,
			int level
			)
		{
			_path = path;
			_level = level;
		}

		public IEnumerable<IEnemy> Create()
		{
			var enemies = new List<IEnemy>();
			switch (_level)
			{
				case 1:
					enemies.Add(new Enemy(_path, 0.05, 5));
					enemies.Add(new Enemy(_path, 0.06, 5));
					enemies.Add(new Enemy(_path, 0.07, 5));
					break;

				case 2:
					enemies.Add(new Enemy(_path, 0.05, 5));
					enemies.Add(new Enemy(_path, 0.06, 5));
					enemies.Add(new Enemy(_path, 0.07, 5));
					enemies.Add(new ArmoredEnemy(_path, 0.017, 5, 3));
					enemies.Add(new ArmoredEnemy(_path, 0.025, 5, 3));
					enemies.Add(new ArmoredEnemy(_path, 0.033, 5, 3));
					break;

				case 3:
					enemies.Add(new Enemy(_path, 0.05, 5));
					enemies.Add(new Enemy(_path, 0.06, 5));
					enemies.Add(new Enemy(_path, 0.07, 5));
					enemies.Add(new ArmoredEnemy(_path, 0.017, 5, 3));
					enemies.Add(new ArmoredEnemy(_path, 0.025, 5, 3));
					enemies.Add(new ArmoredEnemy(_path, 0.033, 5, 3));
					enemies.Add(new SlowingEnemy(_path, 0.11, 5));
					enemies.Add(new SlowingEnemy(_path, 0.12, 5));
					enemies.Add(new SlowingEnemy(_path, 0.13, 5));
					break;

				case 4:
					enemies.Add(new Enemy(_path, 0.05, 5));
					enemies.Add(new Enemy(_path, 0.06, 5));
					enemies.Add(new Enemy(_path, 0.07, 5));
					enemies.Add(new ArmoredEnemy(_path, 0.017, 5, 3));
					enemies.Add(new ArmoredEnemy(_path, 0.025, 5, 3));
					enemies.Add(new ArmoredEnemy(_path, 0.033, 5, 3));
					enemies.Add(new SlowingEnemy(_path, 0.11, 5));
					enemies.Add(new SlowingEnemy(_path, 0.12, 5));
					enemies.Add(new SlowingEnemy(_path, 0.13, 5));
					enemies.Add(new ShakyEnemy(_path, 0.037, 4));
					enemies.Add(new ShakyEnemy(_path, 0.045, 4));
					enemies.Add(new ShakyEnemy(_path, 0.055, 4));
					break;

				case 5:
					enemies.Add(new Enemy(_path, 0.05, 5));
					enemies.Add(new Enemy(_path, 0.06, 5));
					enemies.Add(new Enemy(_path, 0.07, 5));
					enemies.Add(new ArmoredEnemy(_path, 0.017, 5, 3));
					enemies.Add(new ArmoredEnemy(_path, 0.025, 5, 3));
					enemies.Add(new ArmoredEnemy(_path, 0.033, 5, 3));
					enemies.Add(new SlowingEnemy(_path, 0.11, 5));
					enemies.Add(new SlowingEnemy(_path, 0.12, 5));
					enemies.Add(new SlowingEnemy(_path, 0.13, 5));
					enemies.Add(new ShakyEnemy(_path, 0.037, 4));
					enemies.Add(new ShakyEnemy(_path, 0.045, 4));
					enemies.Add(new ShakyEnemy(_path, 0.055, 4));
					enemies.Add(new StoppingEnemy(_path, 0.037, 4));
					enemies.Add(new StoppingEnemy(_path, 0.045, 4));
					enemies.Add(new StoppingEnemy(_path, 0.055, 4));
					break;
			}

			return enemies;
		}
	}
}