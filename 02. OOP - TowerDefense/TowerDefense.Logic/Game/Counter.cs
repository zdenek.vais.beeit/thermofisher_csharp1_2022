﻿using BeeIt.TowerDefense.Contracts.Interfaces;using BeeIt.TowerDefense.Contracts.Math;using System.Collections.Generic;
namespace BeeIt.TowerDefense.Logic.Game{	internal class Counter<T> : ICounter<T>		where T : IEnemy	{		private List<T> _list;
		public Counter()		{			_list = new List<T>();		}		public void Clear()		{			_list.Clear();		}
		public void Add(T element)		{			_list.Add(element);		}
		public void Remove(T element)		{			_list.Remove(element);		}
		public T[] GetAll()		{			return _list.ToArray();		}	}}