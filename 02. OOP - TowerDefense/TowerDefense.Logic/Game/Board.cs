﻿using BeeIt.TowerDefense.Contracts.Graphics;
using BeeIt.TowerDefense.Contracts.Interfaces;

namespace BeeIt.TowerDefense.Logic.Game
{
	internal class Board : IBoard
	{
		public int Width => 10;
		public int Height => 10;
		private ImageType[][] Sprites { get; }

		public Board()
		{
			Sprites = new ImageType[][]
			{
				new ImageType[] { ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_UpDown,		ImageType.Board_Grass,	ImageType.Board_Grass, },
				new ImageType[] { ImageType.Board_DownRight,	ImageType.Board_LeftRight,	ImageType.Board_LeftRight,	ImageType.Board_LeftRight,	ImageType.Board_LeftRight,	ImageType.Board_DownLeft,	ImageType.Board_Grass,		ImageType.Board_UpDown,		ImageType.Board_Grass,	ImageType.Board_Grass, },
				new ImageType[] { ImageType.Board_UpDown,		ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_UpDown,		ImageType.Board_Grass,		ImageType.Board_UpDown,		ImageType.Board_Grass,	ImageType.Board_Grass, },
				new ImageType[] { ImageType.Board_UpRight,		ImageType.Board_LeftRight,	ImageType.Board_LeftRight,	ImageType.Board_DownLeft,	ImageType.Board_Grass,		ImageType.Board_UpDown,		ImageType.Board_Grass,		ImageType.Board_UpDown,		ImageType.Board_Grass,	ImageType.Board_Grass, },
				new ImageType[] { ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_UpDown,		ImageType.Board_Grass,		ImageType.Board_UpRight,	ImageType.Board_LeftRight,	ImageType.Board_UpLeft,		ImageType.Board_Grass,	ImageType.Board_Grass, },
				new ImageType[] { ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_UpDown,		ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_Grass,	ImageType.Board_Grass, },
				new ImageType[] { ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_UpRight,	ImageType.Board_LeftRight,	ImageType.Board_LeftRight,	ImageType.Board_LeftRight,	ImageType.Board_DownLeft,	ImageType.Board_Grass,	ImageType.Board_Grass, },
				new ImageType[] { ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_UpDown,		ImageType.Board_Grass,	ImageType.Board_Grass, },
				new ImageType[] { ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_UpDown,		ImageType.Board_Grass,	ImageType.Board_Grass, },
				new ImageType[] { ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_Grass,		ImageType.Board_UpDown,		ImageType.Board_Grass,	ImageType.Board_Grass, },
			};
		}

		public ImageType GetSpriteType(int x, int y)
		{
			return Sprites[y][x];
		}
	}
}