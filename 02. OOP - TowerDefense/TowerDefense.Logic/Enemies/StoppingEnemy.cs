﻿using BeeIt.TowerDefense.Contracts.Graphics;
using BeeIt.TowerDefense.Logic.Game;

namespace BeeIt.TowerDefense.Logic.Enemies
{
	internal class StoppingEnemy : Enemy
	{
		private readonly double _defaultSpeed;
		private int _sleep;

		public override ImageType Image => ImageType.Enemy_Blue;

		public StoppingEnemy(Path path, double speed, int health)
			: base(path, speed, health)
		{
			_defaultSpeed = speed;
		}

		public override void Move()
		{
			if (_sleep > 0)
			{
				_sleep--;
			}
			else
			{
				Speed = _defaultSpeed;
				base.Move();
			}
		}

		public override void Hit(int hitValue)
		{
			base.Hit(hitValue);
			Speed = 0;
			_sleep = 20;
		}
	}
}