﻿using BeeIt.TowerDefense.Contracts.Graphics;
using BeeIt.TowerDefense.Contracts.Interfaces;
using BeeIt.TowerDefense.Contracts.Math;
using BeeIt.TowerDefense.Logic.Game;

namespace BeeIt.TowerDefense.Logic.Enemies
{
	internal class Enemy : IEnemy
	{
		private readonly Path _path;
		private Checkpoint _currentCheckpoint;

		public Vector Position { get; protected set; }

		public double Speed { get; protected set; }

		public int Health { get; private set; }

		public bool Finished => _currentCheckpoint.Finish;

		public virtual bool Alive => Health > 0;

		public virtual ImageType Image => ImageType.Enemy_Red;

		public Enemy(Path path, double speed, int health)
		{
			_path = path;
			Speed = speed;
			_currentCheckpoint = _path.GetStartCheckpoint();
			Health = health;
			Position = _currentCheckpoint.Position;
		}

		public virtual void Move()
		{
			if (!Finished && Alive)
			{
				_currentCheckpoint = _path.GetCurrentCheckpoint(Position, _currentCheckpoint);
				var direction = (_currentCheckpoint.Position - Position).Normalize();

				Position += direction * Speed;
			}
		}

		public virtual void Hit(int hitValue)
		{
			Health -= hitValue;
		}
	}
}