﻿using BeeIt.TowerDefense.Contracts.Graphics;
using BeeIt.TowerDefense.Contracts.Math;
using BeeIt.TowerDefense.Logic.Game;
using System;

namespace BeeIt.TowerDefense.Logic.Enemies
{
	internal class ShakyEnemy : Enemy
	{
		private readonly Random _random;

		public override ImageType Image => ImageType.Enemy_Green;

		public ShakyEnemy(Path path, double speed, int health)
			: base(path, speed, health)
		{
			_random = new Random(DateTime.UtcNow.Millisecond + (int)(speed * 1000));
		}

		public override void Move()
		{
			Position += GetShakyVector();
			base.Move();
		}

		private Vector GetShakyVector()
		{
			var x = (_random.NextDouble() - 0.5) * 0.1;
			var y = (_random.NextDouble() - 0.5) * 0.1;

			return new Vector(x, y);
		}
	}
}