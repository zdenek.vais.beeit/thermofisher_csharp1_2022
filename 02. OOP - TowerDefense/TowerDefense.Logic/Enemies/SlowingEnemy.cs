﻿using BeeIt.TowerDefense.Contracts.Graphics;
using BeeIt.TowerDefense.Logic.Game;

namespace BeeIt.TowerDefense.Logic.Enemies
{
	internal class SlowingEnemy : Enemy
	{
		public override ImageType Image => ImageType.Enemy_Orange;

		public SlowingEnemy(Path path, double speed, int health)
			: base(path, speed, health)
		{
		}

		public override void Hit(int hitValue)
		{
			Speed *= 0.5;
			base.Hit(hitValue);
		}
	}
}