﻿using BeeIt.TowerDefense.Contracts.Graphics;
using BeeIt.TowerDefense.Logic.Game;
using System.Collections.Generic;

namespace BeeIt.TowerDefense.Logic.Enemies
{
	internal class ArmoredEnemy : Enemy
	{
		private Dictionary<int, ImageType> _colors = new Dictionary<int, ImageType>
		{
			{3, ImageType.Enemy_Red3},
			{2, ImageType.Enemy_Red2},
			{1, ImageType.Enemy_Red1},
			{0, ImageType.Enemy_Red},
		};

		public int Armor { get; private set; }

		public override ImageType Image => _colors[Armor];

		public ArmoredEnemy(Path path, double speed, int health, int armor)
			: base(path, speed, health)
		{
			Armor = armor;
		}

		public override void Hit(int hitValue)
		{
			if (Armor > 0)
			{
				Armor -= hitValue;
			}
			else
			{
				base.Hit(hitValue);
			}
		}
	}
}