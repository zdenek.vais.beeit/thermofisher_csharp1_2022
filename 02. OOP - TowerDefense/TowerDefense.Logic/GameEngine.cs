﻿using BeeIt.TowerDefense.Contracts.Graphics;
using BeeIt.TowerDefense.Contracts.Interfaces;
using BeeIt.TowerDefense.Logic.Factories;
using BeeIt.TowerDefense.Logic.Game;
using BeeIt.TowerDefense.Contracts.Math;
using BeeIt.TowerDefense.Logic.Enemies;
namespace BeeIt.TowerDefense.Logic
{
	public class GameEngine
	{
		private readonly Path _path;
		private readonly IDrawEngine _draw;
		private readonly IBoard _board;
		private readonly IEnemyFactory _enemyFactory;
		private readonly ITowerFactory _towersFactory;
		private readonly Statistics _statistics;

		public GameEngine(
			IDrawEngine draw
			)
		{
			_draw = draw;
			_path = new Path();
			_board = new Board();
			_enemyFactory = new EnemyFactory(_path, 5);
			_towersFactory = new TowerFactory();
			_statistics = new Statistics();
		}

		public void Run()
		{
			//CastExample();

			while (true)
			{
				var enemies = _enemyFactory.Create();
				var towers = _towersFactory.Create();

				for (var i = 0; i < 630; i++)
				{
					for (var row = 0; row < _board.Height; row++)					{						for (var column = 0; column < _board.Width; column++)						{							var imageType = _board.GetSpriteType(column, row);							_draw.Draw(imageType, new Vector(column, row));						}					}

					foreach (var enemy in enemies)
					{
						if (enemy.Alive)
						{
							enemy.Move();
							_draw.Draw(enemy.Image, enemy.Position);
						}
					}

					foreach (var tower in towers)
					{
						tower.LocateEnemies(enemies);
						_draw.Draw(ImageType.Tower, tower.Position, tower.Angle);
						if (tower.HitTarget != null)
						{
							_draw.Draw(ImageType.Explosion, tower.HitTarget);
						}
					}

					var hitPoints = _statistics.CalculateHitPoints(enemies);
					_draw.Print("Hits left: " + hitPoints);
					_draw.Refresh();
				}
			}
		}
		// ============================================================================================================================		// A. Abstrakce na úrovni dědičnosti		// Vytvoříme dvě proměnné:		//  a) parent typu Enemy		//  b) child typu ArmoredEnemy		// ArmoredEnemy dědí od Enemy -> ArmoredEnemy : Enemy		//		// pro každý řádek a2-b7 odpovězte na otázku:		//  a) jaký má výsledek typ: Enemy nebo ArmoredEnemy		//  b) jaká je výsledná hodnota: parent, child, true, false, null		// ============================================================================================================================
		private void CastExample()		{			Enemy parent = new Enemy(null, 0, 0);			ArmoredEnemy child = new ArmoredEnemy(null, 0, 0, 0);
			//											Typ						Hodnota
			var a2 = (Enemy)parent;                 //	Enemy|ArmoredEnemy		parent|child|true|false|null
			var a3 = (ArmoredEnemy)parent;          //	Enemy|ArmoredEnemy		parent|child|true|false|null
			var a4 = parent is Enemy;               //	Enemy|ArmoredEnemy		parent|child|true|false|null
			var a5 = parent is ArmoredEnemy;        //	Enemy|ArmoredEnemy		parent|child|true|false|null
			var a6 = parent as Enemy;               //	Enemy|ArmoredEnemy		parent|child|true|false|null
			var a7 = parent as ArmoredEnemy;        //	Enemy|ArmoredEnemy		parent|child|true|false|null
			var b2 = (Enemy)child;                  //	Enemy|ArmoredEnemy		parent|child|true|false|null
			var b3 = (ArmoredEnemy)child;           //	Enemy|ArmoredEnemy		parent|child|true|false|null
			var b4 = child is ArmoredEnemy;         //	Enemy|ArmoredEnemy		parent|child|true|false|null
			var b5 = child is Enemy;                //	Enemy|ArmoredEnemy		parent|child|true|false|null
			var b6 = child as ArmoredEnemy;         //	Enemy|ArmoredEnemy		parent|child|true|false|null
			var b7 = child as Enemy;                //	Enemy|ArmoredEnemy		parent|child|true|false|null

			b2.Move();		}
	}
}
