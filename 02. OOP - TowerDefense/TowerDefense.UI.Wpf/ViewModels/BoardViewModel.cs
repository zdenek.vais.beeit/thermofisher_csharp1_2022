﻿using BeeIt.TowerDefense.Contracts.Graphics;
using BeeIt.TowerDefense.Contracts.Math;
using BeeIt.TowerDefense.UI.Resources;using BeeIt.TowerDefense.UI.Wpf.Models;
using System.Collections.Generic;

namespace BeeIt.TowerDefense.UI.Wpf.ViewModels
{
	internal class BoardViewModel : ViewModelBase
	{
		private string _message;

		public BoardViewModel(int width, int height)
		{
			BoardImages = new ImageType[height][];
			for (var y = 0; y < height; y++)
			{
				BoardImages[y] = new ImageType[width];
			}
			MovingElements = new List<MovingElement>();
		}

		public ImageType[][] BoardImages { get; set; }

		public IList<MovingElement> MovingElements { get; }

		public string Message 
		{
			get => _message;
			set			{				_message = value;				OnPropertyChanged(nameof(Message));			} 
		}

		public void Update(Vector position, ImageType type, double angle)
		{
			if (type.ToString().StartsWith("Board"))			{				BoardImages[(int)position.Y][(int)position.X] = type;			}
			else			{				var bitmap = ImageMapper.MapElement(type);				MovingElements.Add(new MovingElement				{					Position = position,					Angle = angle,					Image = bitmap,				});			}
		}

		public void Clear()		{			OnPropertyChanged(nameof(BoardImages));			MovingElements.Clear();		}
	}
}
