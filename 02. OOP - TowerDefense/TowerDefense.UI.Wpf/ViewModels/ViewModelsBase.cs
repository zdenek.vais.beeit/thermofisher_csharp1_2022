﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BeeIt.TowerDefense.UI.Wpf.ViewModels
{
	internal abstract class ViewModelBase : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		public virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
