﻿using BeeIt.TowerDefense.Contracts.Math;
using System.Drawing;

namespace BeeIt.TowerDefense.UI.Wpf.Models
{
	internal class MovingElement
	{
		public Vector Position { get; set; }
		public double Angle { get; set; }
		public Bitmap Image { get; set; }
	}
}