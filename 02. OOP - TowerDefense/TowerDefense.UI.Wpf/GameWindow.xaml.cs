﻿using BeeIt.TowerDefense.UI.Wpf.Converters;
using BeeIt.TowerDefense.UI.Wpf.Models;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace BeeIt.TowerDefense.UI.Wpf
{
	internal partial class GameWindow : Window
	{
		private readonly IValueConverter _converter;
		private double _tileX, _tileY;

		public GameWindow()
		{
			InitializeComponent();
			_converter = new BitmapToImageSourceConverter();

			_tileX = 100;
			_tileY = 100;
		}

		public void Refresh(IEnumerable<MovingElement> movingElements)
		{
			MyCanvas.Children.Clear();

			foreach (var element in movingElements)
			{
				if (element?.Image != null)
				{
					var source = (BitmapSource)_converter.Convert(element.Image, typeof(ImageSource), null, null);
					var s = new Image
					{
						Source = source,
					};

					MyCanvas.Children.Add(s);

					s.RenderTransform = new RotateTransform(element.Angle, source.Width / 2, source.Height / 2);
					Canvas.SetTop(s, element.Position.Y * _tileY + 12);
					Canvas.SetLeft(s, element.Position.X * _tileX + 8);
				}
			}
		}
	}
}
