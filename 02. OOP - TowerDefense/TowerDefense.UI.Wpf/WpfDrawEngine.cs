﻿using BeeIt.TowerDefense.Contracts.Graphics;
using BeeIt.TowerDefense.Contracts.Interfaces;
using BeeIt.TowerDefense.Contracts.Math;using BeeIt.TowerDefense.UI.Wpf.ViewModels;
using System;
using System.Linq;
using System.Threading;
using System.Windows.Threading;

namespace BeeIt.TowerDefense.UI.Wpf
{
	public class WpfDrawEngine : IDrawEngine
	{
        private bool _started;
		private GameWindow _gameWindow;
        private readonly Thread _uiThread;
        private readonly ManualResetEventSlim _uiThreadInitialized;
		private readonly ManualResetEventSlim _engineThreadInitialized;
        public WpfDrawEngine(int width, int height)
		{
            _uiThreadInitialized = new ManualResetEventSlim();
			_engineThreadInitialized = new ManualResetEventSlim();			ViewModelsList.Board = new BoardViewModel(width, height);
			_uiThread = new Thread(() =>
            {
                try
                {
                    _gameWindow = new GameWindow();
                    _uiThreadInitialized.Set();
					_engineThreadInitialized.Wait();					_gameWindow.Show();
					// start the Dispatcher processing					Dispatcher.Run();
                }
                catch (Exception)
                {
                    _uiThreadInitialized.Set();
					throw;
                }
            });

			// Set the apartment state to allow WPF to show Windows			_uiThread.SetApartmentState(ApartmentState.STA);
			_uiThread.Start();		}

		public void Draw(ImageType type, Vector position, double angle = 0)		{            RunInDispatherThread(() => ViewModelsList.Board.Update(position, type, angle));		}

		public void Print(string message)		{			RunInDispatherThread(() => ViewModelsList.Board.Message = message);		}

		public void Refresh()
		{
			RunInDispatherThread(() => _gameWindow.Refresh(ViewModelsList.Board.MovingElements.ToList()));
            Thread.Sleep(50);			ViewModelsList.Board.Clear();
			if (!_started)			{				_engineThreadInitialized.Set();				_started = true;			}		}

        private void RunInDispatherThread(Action action)
        {
            if (!_gameWindow?.Dispatcher.HasShutdownStarted ?? false)
            {
                // Do not wait for UI change to finish
                _gameWindow?.Dispatcher.BeginInvoke(action);
            }
        }	}
}
