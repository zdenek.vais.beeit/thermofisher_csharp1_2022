﻿using BeeIt.TowerDefense.Contracts.Graphics;using BeeIt.TowerDefense.Contracts.Math;

namespace BeeIt.TowerDefense.Contracts.Interfaces
{
	public interface IDrawEngine
	{
		void Draw(ImageType type, Vector position, double angle = 0);
		void Print(string message);
		void Refresh();
	}
}