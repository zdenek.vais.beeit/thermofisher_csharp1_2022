﻿using BeeIt.TowerDefense.Contracts.Graphics;
using BeeIt.TowerDefense.Contracts.Math;

namespace BeeIt.TowerDefense.Contracts.Interfaces
{
	public interface IEnemy
	{
		Vector Position { get; }
		double Speed { get; }
		int Health { get; }
		bool Finished { get; }
		bool Alive { get; }
		ImageType Image { get; }
		void Move();
		void Hit(int hitValue);
	}
}