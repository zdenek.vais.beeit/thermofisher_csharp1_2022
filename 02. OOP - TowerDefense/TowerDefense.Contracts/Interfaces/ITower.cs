﻿using BeeIt.TowerDefense.Contracts.Math;
using System.Collections.Generic;

namespace BeeIt.TowerDefense.Contracts.Interfaces
{
	public interface ITower
	{
		Vector Position { get; }
		double Angle { get; }
		Vector HitTarget { get; }
		void LocateEnemies(IEnumerable<IEnemy> enemies);
	}
}