﻿using System.Collections.Generic;

namespace BeeIt.TowerDefense.Contracts.Interfaces
{
	//====================================================================================================================
	// C. Generika
	//====================================================================================================================
	// 1. Všimněte si, že IEnemyFactory a ITowerFactory jsou skoro totožné.
	//		- V tomhleto případě nemůžeme použít dědičnost. Proč?
	// 2. Napište nový interface IGameFactory<T> s generickym typem T, který bude vracet IEnumerable<T>
	// 3. Přidejte tyto dva interfacy do implementací typů EnemyFactory a TowerFactory
	// 4. V souboru GameEngine.cs změňte typy proměnných:
	//		- _enemyFactory na IGameFactory<IEnemy>
	//		- _towerFactory na IGameFactory<ITower>
	// 5. Můžeme smazat interfacy IEnemyFactory a ITowerFactory?	
	//
	//====================================================================================================================

	public interface IEnemyFactory
	{
		IEnumerable<IEnemy> Create();
	}

	public interface ITowerFactory
	{
		IEnumerable<ITower> Create();
	}
}
