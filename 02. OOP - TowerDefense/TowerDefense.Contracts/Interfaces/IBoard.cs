﻿using BeeIt.TowerDefense.Contracts.Graphics;

namespace BeeIt.TowerDefense.Contracts.Interfaces
{
	public interface IBoard
	{
		int Width { get; }
		int Height { get; }
		ImageType GetSpriteType(int x, int y);
	}
}