﻿namespace BeeIt.TowerDefense.Contracts.Graphics
{
	public enum ImageType
	{
		Board_Grass,
		Board_Center,
		Board_UpLeft,
		Board_UpRight,
		Board_DownLeft,
		Board_DownRight,
		Board_LeftRight,
		Board_UpDown,
		Enemy_Red,
		Enemy_Red1,
		Enemy_Red2,
		Enemy_Red3,
		Enemy_Green,
		Enemy_Orange,
		Enemy_Blue,
		Enemy_Yellow,
		Enemy_Brown,
		Tower,
		Explosion
	}
}