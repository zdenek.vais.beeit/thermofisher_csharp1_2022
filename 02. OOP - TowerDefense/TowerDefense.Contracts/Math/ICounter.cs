﻿using BeeIt.TowerDefense.Contracts.Interfaces;
namespace BeeIt.TowerDefense.Contracts.Math{	public interface ICounter<T>		where T : IEnemy	{		void Clear();
		void Add(T element);
		void Remove(T element);
		T[] GetAll();	}}