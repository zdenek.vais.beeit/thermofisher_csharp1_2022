﻿using BeeIt.TowerDefense.Logic;
using BeeIt.TowerDefense.Logic.Game;
using BeeIt.TowerDefense.UI.Wpf;
using BeeIt.TowerDefense.UI.Console;

namespace BeeIt.TowerDefense.Main
{
	internal class Program
	{
		public static void Main(string[] args)
		{
			// ============================================================================================================================
			// B. Abstrakce na úrovni projektu
			// Vyzkoušejte nastavit proměnnou draw na jiné implementace 
			// ============================================================================================================================
			var draw = new ConsoleDrawEngine(10, 10);
			//var draw = new WpfDrawEngine(10, 10);
			//var draw = new AsciiArtDrawEngine(10, 10);
			var gameLoop = new GameEngine(draw);
			gameLoop.Run();

			System.Console.ReadKey();
		}
	}
}