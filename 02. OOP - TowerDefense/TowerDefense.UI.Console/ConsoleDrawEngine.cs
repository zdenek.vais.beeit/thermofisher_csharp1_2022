﻿using BeeIt.TowerDefense.Contracts.Graphics;using BeeIt.TowerDefense.Contracts.Interfaces;
using BeeIt.TowerDefense.Contracts.Math;
using BeeIt.TowerDefense.UI.Resources;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;

namespace BeeIt.TowerDefense.UI.Console
{
	public class ConsoleDrawEngine : IDrawEngine, IDisposable
	{
		private const int TILE_SIZE = 64;
		private readonly int _width;
		private readonly int _height;
		private Bitmap _currentBuffer;

		public ConsoleDrawEngine(int width, int height)
		{
			System.Console.BufferWidth = System.Console.WindowWidth = 70;
			System.Console.BufferHeight = System.Console.WindowHeight = 39;
			System.Console.CursorVisible = false;

			_width = width;
			_height = height;
			_currentBuffer = new Bitmap(_width * TILE_SIZE, _height * TILE_SIZE);
		}

		public void Draw(ImageType type, Vector position, double angle = 0)		{			var bitmap = ImageMapper.MapElement(type);			DrawPicture(position.X, position.Y, bitmap, angle);		}

		public void Print(string message)		{			using (var g = Graphics.FromImage(_currentBuffer))			{				var font = new Font("Arial", 16);				var drawBrush = new SolidBrush(Color.Black);				g.DrawString(message, font, drawBrush, 0, 0);			}		}

		public void Refresh()
		{
			using (var g = Graphics.FromHwnd(GetConsoleWindow()))
			{
				g.DrawImage(_currentBuffer, new Rectangle(0, 0, _width * TILE_SIZE, _height * TILE_SIZE));
			}
			_currentBuffer = new Bitmap(_width * TILE_SIZE, _height * TILE_SIZE);
		}

		public void Dispose()
		{
			_currentBuffer?.Dispose();
		}

		[DllImport("kernel32.dll", SetLastError = true)]
		private static extern IntPtr GetConsoleWindow();

		private void DrawPicture(double x, double y, Bitmap sprite, double rotationAngle = 0)
		{
			using (var g = Graphics.FromImage(_currentBuffer))
			{
				g.TranslateTransform((float)x * TILE_SIZE, (float)y * TILE_SIZE);
				g.TranslateTransform((float)TILE_SIZE / 2, (float)TILE_SIZE / 2);
				g.RotateTransform((float)rotationAngle);
				g.InterpolationMode = InterpolationMode.NearestNeighbor;

				var imageRect = new Rectangle(-TILE_SIZE / 2, -TILE_SIZE / 2, TILE_SIZE, TILE_SIZE);
				g.DrawImage(sprite, imageRect);
			}
		}	}
}