﻿using BeeIt.TowerDefense.Contracts.Graphics;
using System.Collections.Generic;
using System.Drawing;

namespace BeeIt.TowerDefense.UI.Resources
{
	public static class ImageMapper
	{
		private static Dictionary<ImageType, Bitmap> _enemyMap = new Dictionary<ImageType, Bitmap>
		{
			{ ImageType.Enemy_Red, ElementImages.Enemy_Red },
			{ ImageType.Enemy_Red1, ElementImages.Enemy_Red_armor1 },
			{ ImageType.Enemy_Red2, ElementImages.Enemy_Red_armor2 },
			{ ImageType.Enemy_Red3, ElementImages.Enemy_Red_armor3 },
			{ ImageType.Enemy_Orange, ElementImages.Enemy_Orange },
			{ ImageType.Enemy_Blue, ElementImages.Enemy_Blue },
			{ ImageType.Enemy_Green, ElementImages.Enemy_Green },
			{ ImageType.Enemy_Yellow, ElementImages.Enemy_Yellow },
			{ ImageType.Enemy_Brown, ElementImages.Enemy_Brown },
			{ ImageType.Tower, ElementImages.Tower },
			{ ImageType.Explosion, ElementImages.Explosion },
			{ ImageType.Board_Grass, BoardImages.Ground_Grass },
			{ ImageType.Board_Center,BoardImages.Ground_Center },
			{ ImageType.Board_UpLeft,BoardImages.Ground_Up_Left },
			{ ImageType.Board_UpRight,BoardImages.Ground_Up_Right },
			{ ImageType.Board_DownLeft,BoardImages.Ground_Down_Left },
			{ ImageType.Board_DownRight,BoardImages.Ground_Down_Right },
			{ ImageType.Board_LeftRight,BoardImages.Ground_Left_Right },
			{ ImageType.Board_UpDown, BoardImages.Ground_Up_Down },
		};

		public static Bitmap MapElement(ImageType color)
		{
			return _enemyMap[color];
		}
	}
}