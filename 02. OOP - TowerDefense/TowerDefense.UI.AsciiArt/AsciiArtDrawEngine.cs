﻿using BeeIt.TowerDefense.Contracts.Graphics;using BeeIt.TowerDefense.Contracts.Interfaces;
using BeeIt.TowerDefense.Contracts.Math;
using BeeIt.TowerDefense.UI.Resources;
using System;using System.Drawing;

namespace BeeIt.TowerDefense.UI.Console
{
	public class AsciiArtDrawEngine : IDrawEngine
	{
		private const int TILE_SIZE = 64;
		private readonly int _width;
		private readonly int _height;
		private char[][] _board;

		public AsciiArtDrawEngine(int width, int height)
		{
			_width = width;
			_height = height;
			_board = new char[_height][];
			for (var i = 0; i < height; i++)			{				_board[i] = new char[_width];			}
			Clear();
		}

		public void Draw(ImageType type, Vector position, double angle = 0)		{			if (type.ToString().StartsWith("Enemy"))			{				_board[(int)Math.Round(position.Y)][(int)Math.Round(position.X)] = 'x';			}
			if (type.ToString().StartsWith("Tower"))			{				_board[(int)Math.Round(position.Y)][(int)Math.Round(position.X)] = 'o';			}		}

		public void Print(string message)		{		}

		public void Refresh()
		{
			System.Console.Clear();

			System.Console.Write('|');
			for (var column = 0; column < _width; column++)			{				System.Console.Write('-');			}
			System.Console.WriteLine('|');
			for (var row = 0; row < _height; row++)			{				System.Console.Write('|');
				for (var column = 0; column < _width; column++)				{					System.Console.Write(_board[row][column]);				}
				System.Console.Write('|');				System.Console.WriteLine();			}
			System.Console.Write('|');
			for (var column = 0; column < _width; column++)			{				System.Console.Write('-');			}
			System.Console.WriteLine('|');
			Clear();
			System.Threading.Thread.Sleep(5);
		}

		private void Clear()		{			for (var row = 0; row < _height; row++)			{				for (var column = 0; column < _width; column++)				{					_board[row][column] = ' ';				}			}		}
	}
}