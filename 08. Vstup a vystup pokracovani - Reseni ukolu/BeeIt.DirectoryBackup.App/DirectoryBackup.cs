﻿namespace BeeIt.DirectoryBackup;

internal class DirectoryBackup
{
    public static void BackupDirectory(string sourceDirPath, string destinationDirPath)
    {
        if (string.IsNullOrWhiteSpace(sourceDirPath)) throw new ArgumentException("Path cannot be null or empty.", nameof(sourceDirPath));
        if (string.IsNullOrWhiteSpace(destinationDirPath)) throw new ArgumentException("Path cannot be null or empty.", nameof(destinationDirPath));

        SafeIO.CreateDirectory(destinationDirPath);

        string[] directoryPathesToCopy = SafeIO.GetSubDirectories(sourceDirPath);
        string[] filePathesToCopy = SafeIO.GetFiles(sourceDirPath);

        // Kopirovani slozek
        foreach (var sourceSubDirPath in directoryPathesToCopy)
        {
            var relativePath = Path.GetRelativePath(sourceDirPath, sourceSubDirPath);
            var destinationSubDirPath = Path.Combine(destinationDirPath, relativePath);

            var result = SafeIO.CreateDirectory(destinationSubDirPath);
            if (result)
            {
                Console.WriteLine($"Directory '{destinationSubDirPath}' created.");
            }
        }

        // Kopirovani souboru
        foreach (var sourceFilePath in filePathesToCopy)
        {
            var relativePath = Path.GetRelativePath(sourceDirPath, sourceFilePath);
            var destinationFilePath = Path.Combine(destinationDirPath, relativePath);

            var result = SafeIO.CopyFile(sourceFilePath, destinationFilePath);

            if (result)
            {
                Console.WriteLine($"{sourceFilePath} copied.");
            }
        }

    }
}

