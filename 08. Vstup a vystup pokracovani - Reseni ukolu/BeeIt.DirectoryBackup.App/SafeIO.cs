﻿namespace BeeIt.DirectoryBackup;

public class SafeIO
{
    public static string[] GetSubDirectories(string directoryPath, string searchPattern = "*", SearchOption searchOption = SearchOption.AllDirectories)
    {
        if (string.IsNullOrWhiteSpace(directoryPath)) throw new ArgumentException("Path cannot be null or empty.", nameof(directoryPath));

        try
        {
            return Directory.GetDirectories(directoryPath, searchPattern, searchOption);
        }
        catch (UnauthorizedAccessException ex)
        {
            Console.WriteLine($"Insufficient permissions for directory '{directoryPath}': {ex}");
        }
        catch (PathTooLongException ex)
        {
            Console.WriteLine($"Path '{directoryPath}' is too long: {ex}");
        }
        catch (DirectoryNotFoundException ex)
        {
            Console.WriteLine($"Directory was not found: {ex}");
        }
        catch (IOException ex)
        {
            Console.WriteLine($"Unspecified I/O exception occurred when copying a file: {ex}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Unexpected exception occurred when copying a file: {ex}");
        }

        return null;
    }

    public static string[] GetFiles(string directoryPath, string searchPattern = "*", SearchOption searchOption = SearchOption.AllDirectories)
    {
        if (string.IsNullOrWhiteSpace(directoryPath)) throw new ArgumentException("Path cannot be null or empty.", nameof(directoryPath));

        try
        {
            return Directory.GetFiles(directoryPath, searchPattern, searchOption);
        }
        catch (UnauthorizedAccessException ex)
        {
            Console.WriteLine($"Insufficient permissions for directory '{directoryPath}': {ex}");
        }
        catch (PathTooLongException ex)
        {
            Console.WriteLine($"Path '{directoryPath}' is too long: {ex}");
        }
        catch (DirectoryNotFoundException ex)
        {
            Console.WriteLine($"Directory was not found: {ex}");
        }
        catch (IOException ex)
        {
            Console.WriteLine($"Unspecified I/O exception occurred when copying a file: {ex}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Unexpected exception occurred when copying a file: {ex}");
        }

        return null;
    }

    public static bool CreateDirectory(string directoryPathToCreate)
    {
        if (string.IsNullOrWhiteSpace(directoryPathToCreate)) throw new ArgumentException("Path cannot be null or empty.", nameof(directoryPathToCreate));

        try
        {
            Directory.CreateDirectory(directoryPathToCreate);
            return true;
        }
        catch (UnauthorizedAccessException ex)
        {
            Console.WriteLine($"Insufficient permissions for directory '{directoryPathToCreate}': {ex}");
        }
        catch (PathTooLongException ex)
        {
            Console.WriteLine($"Path '{directoryPathToCreate}' is too long: {ex}");
        }
        catch (DirectoryNotFoundException ex)
        {
            Console.WriteLine($"Directory was not found: {ex}");
        }
        catch (IOException ex)
        {
            Console.WriteLine($"Unspecified I/O exception occurred when copying a file: {ex}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Unexpected exception occurred when copying a file: {ex}");
        }

        return false;
    }

    public static bool CopyFile(string sourceFilePath, string destinationFilePath, int bufferSize = 1024)
    {
        if (string.IsNullOrWhiteSpace(sourceFilePath)) throw new ArgumentException("Path cannot be null or empty.", nameof(sourceFilePath));
        if (string.IsNullOrWhiteSpace(destinationFilePath)) throw new ArgumentException("Path cannot be null or empty.", nameof(destinationFilePath));

        int bytesRead = 0;
        var buffer = new byte[bufferSize];

        try
        {
            using var streamRead = new FileStream(sourceFilePath, FileMode.Open, FileAccess.Read);
            using var streamWrite = new FileStream(destinationFilePath, FileMode.Create, FileAccess.Write);

            while ((bytesRead = streamRead.Read(buffer, 0, buffer.Length)) > 0)
            {
                streamWrite.Write(buffer, 0, bytesRead);
            }

            return true;
        }
        catch (UnauthorizedAccessException ex)
        {
            Console.WriteLine($"Insufficient permissions: {ex}");
        }
        catch (FileNotFoundException ex)
        {
            Console.WriteLine($"File was not found: {ex}");
        }
        catch (DirectoryNotFoundException ex)
        {
            Console.WriteLine($"Directory was not found: {ex}");
        }
        catch (IOException ex)
        {
            Console.WriteLine($"Unspecified I/O exception occurred when copying a file: {ex}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Unexpected exception occurred when copying a file: {ex}");
        }

        return false;
    }
}

