﻿using System.ComponentModel;

namespace BeeIt.Functory.Api.Models
{
	public enum ExoplanetFields
	{
		[Description("pl_hostname")]
		HostName,
		[Description("pl_name")]
		Name,
		[Description("pl_orbper")]
		OrbitalPeriodInDays,
		[Description("st_dist")]
		DistanceInParsecs,
		[Description("ra")]
		RightAscensionInDegrees,
		[Description("dec")]
		DeclinationInDegrees
	}
}