﻿using Newtonsoft.Json;

namespace BeeIt.Functory.Api.Models
{
	public class Exoplanet
	{
		[JsonProperty("pl_hostname")]
		public string HostName { get; set; }

		[JsonProperty("pl_name")]
		public string Name { get; set; }

		[JsonProperty("pl_orbper")]
		public double? OrbitalPeriodInDays { get; set; }

		[JsonProperty("st_dist")]
		public double? DistanceInParsecs { get; set; }

		[JsonProperty("ra")]
		public double? RightAscensionInDegrees { get; set; }

		[JsonProperty("dec")]
		public double? DeclinationInDegrees { get; set; }
	}
}
