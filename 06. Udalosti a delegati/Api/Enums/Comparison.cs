﻿namespace BeeIt.Functory.Api.Enums
{
	public enum Comparison
	{
		LESS_THAN,
		GREATER_THAN,
	}
}