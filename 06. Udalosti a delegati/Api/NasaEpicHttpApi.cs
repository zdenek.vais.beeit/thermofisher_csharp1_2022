﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net.Http;
using Newtonsoft.Json;
using BeeIt.Functory.Api.Models;
using BeeIt.Functory.Api.Enums;

namespace BeeIt.Functory.Api
{
	public class NasaEpicHttpApi
	{
		private const string _url_epic_host = "https://epic.gsfc.nasa.gov";
		private const string _url_exoplanet_host = "https://exoplanetarchive.ipac.caltech.edu/cgi-bin/nstedAPI/nph-nstedAPI";
		//private const string _url_exoplanet_query = "?table=cumulative&select=pl_hostname,pl_name,pl_orbper,st_dist,st_optmag,ra,dec&order=dec&format=json";
		private const string _url_exoplanet_query = "?table=cumulative&format=json";
		private readonly int _type;
		private int _attempts;

		public TimeSpan Timeout { get; set; }
		public string Url { get; set; }

		public NasaEpicHttpApi(int type)
		{
			_type = type;
			_attempts = 0;
			Timeout = TimeSpan.FromMinutes(1);
		}

		public byte[] GetPicture()
		{
			return CallApiBytes("/archive/natural/2020/03/31/png/epic_1b_20200331003634.png");
		}

		public IEnumerable<Exoplanet> GetAll()
		{
			return CallApi<List<Exoplanet>>(_url_exoplanet_query);
		}

		public IEnumerable<Exoplanet> GetExoplanetsByFilter(ExoplanetFields exoplanetField, Comparison comparison, object value)
		{
			var field = GetDescription(exoplanetField);
			var comparisonSign = comparison == Comparison.GREATER_THAN ? ">" : "<";
			return CallApi<List<Exoplanet>>(_url_exoplanet_query + $"&where={field}{comparisonSign}{value}");
		}

		public IEnumerable<Exoplanet> GetExoplanetsByName(string name)
		{
			return CallApi<List<Exoplanet>>(_url_exoplanet_query + $"&where=pl_hostname like '%{name}%'");
		}

		private T CallApi<T>(string method)
		{
			_attempts++;
			if (_type == 1 && _attempts <= 1)
			{
				throw new TimeoutException("Some timeout!");
			}
			if (_type == 2 && _attempts <= 3)
			{
				throw new TimeoutException("Some timeout!");
			}
			if (_type == 3 && _attempts <= 3)
			{
				throw new InvalidDataException("Unknown error");
			}

			string result;
			using (var client = new HttpClient())
			{
				client.Timeout = Timeout;
				Url = _url_exoplanet_host + method;
				result = client.GetStringAsync(Url).GetAwaiter().GetResult();
			}
			_attempts = 0;
			//result = result.Substring(result.IndexOf("["));
			return JsonConvert.DeserializeObject<T>(result);
		}

		private byte[] CallApiBytes(string method)
		{
			byte[] data;
			using (var client = new HttpClient())
			{
				client.Timeout = TimeSpan.FromMinutes(10);
				data = client.GetByteArrayAsync(_url_epic_host + method).GetAwaiter().GetResult();
			}
			return data;
		}

		private string GetDescription<T>(T source)
		{
			var fi = source.GetType().GetField(source.ToString());
			var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
			return attributes.Length > 0 ? attributes[0].Description : source.ToString();
		}
	}
}
