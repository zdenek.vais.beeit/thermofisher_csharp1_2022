﻿using System;
using System.Collections.Generic;
using BeeIt.Functory.Api.Enums;
using BeeIt.Functory.Api.Models;

namespace BeeIt.Functory.Func
{
	internal interface INasaEpicApi
	{
		event Action<string> CallSuccess;
		event Action<string> CallFailed;
		IEnumerable<Exoplanet> VratExoplanety();
		IEnumerable<Exoplanet> VratExoplanety(ExoplanetFields exoplanetField, Comparison comparison, object value);
		IEnumerable<Exoplanet> VratExoplanety(string name);
	}
}