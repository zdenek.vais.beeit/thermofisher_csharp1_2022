﻿using System;
using System.Collections.Generic;
using BeeIt.Functory.Api;
using BeeIt.Functory.Api.Enums;
using BeeIt.Functory.Api.Models;

namespace BeeIt.Functory.Func.Wrappers
{
	internal class NasaApiClassesWithFuncHandling : INasaEpicApi
	{
		private readonly NasaEpicHttpApi _daveApi;

		public event Action<string> CallSuccess;
		public event Action<string> CallFailed;

		public NasaApiClassesWithFuncHandling(int type)
		{
			_daveApi = new NasaEpicHttpApi(type);
		}

		public IEnumerable<Exoplanet> VratExoplanety()
		{
			// TODO: Volej Boba s kodem Alice pro vsechny exoplanety
			return null;
		}

		public IEnumerable<Exoplanet> VratExoplanety(ExoplanetFields exoplanetField, Comparison comparison, object value)
		{
			// TODO: Volej Boba s kodem Alice pro vybrane exoplanety podle porovnani
			return null;
		}

		public IEnumerable<Exoplanet> VratExoplanety(string name)
		{
			// TODO: Volej Boba s kodem Alice pro exoplanety podle nazvu
			return null;
		}

		private IEnumerable<Exoplanet> VolejBoba(/*TODO argument functor od Alice*/)
		{
			var retries = 5;
			var timeouts = 0;
			Exception lastException = null;
			while (retries > 0)
			{
				try
				{
					IEnumerable<Exoplanet> output = null;

					// TODO: Volej kod Alice

					if (CallSuccess != null)
					{
						CallSuccess.Invoke(_daveApi.Url);
					}
					return output;
				}
				catch (TimeoutException e)
				{
					Console.WriteLine("Increasing timeout");
					_daveApi.Timeout = _daveApi.Timeout.Add(TimeSpan.FromMinutes(1));
					lastException = new TimeoutException($"Timeouted for {timeouts} time!", e);
					timeouts++;
					CallFailed?.Invoke(_daveApi.Url);
				}
				catch (Exception e)
				{
					lastException = e;
					Console.WriteLine(e.Message);
					CallFailed?.Invoke(_daveApi.Url);
				}

				retries--;
			}

			throw lastException;
		}
	}
}