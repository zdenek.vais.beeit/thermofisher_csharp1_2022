﻿using System;
using System.Collections.Generic;
using BeeIt.Functory.Api;
using BeeIt.Functory.Api.Enums;
using BeeIt.Functory.Api.Models;

namespace BeeIt.Functory.Func.Wrappers
{
	internal class NasaApiSimple : INasaEpicApi
	{
		private readonly NasaEpicHttpApi _httpApi;

		public event Action<string> CallSuccess;
		public event Action<string> CallFailed;

		public NasaApiSimple(int type)
		{
			_httpApi = new NasaEpicHttpApi(type);
		}

		public IEnumerable<Exoplanet> VratExoplanety()
		{
			return _httpApi.GetAll();
		}

		public IEnumerable<Exoplanet> VratExoplanety(ExoplanetFields exoplanetField, Comparison comparison, object value)
		{
			return _httpApi.GetExoplanetsByFilter(exoplanetField, comparison, value);
		}

		public IEnumerable<Exoplanet> VratExoplanety(string name)
		{
			return _httpApi.GetExoplanetsByName(name);
		}
	}
}