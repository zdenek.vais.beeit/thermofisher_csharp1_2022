﻿using System;
using System.Collections.Generic;
using BeeIt.Functory.Api;
using BeeIt.Functory.Api.Enums;
using BeeIt.Functory.Api.Models;

namespace BeeIt.Functory.Func.Wrappers
{
	internal interface IApiCall
	{
		IEnumerable<Exoplanet> CallApi(NasaEpicHttpApi httpApi);
	}

	internal class ApiCallGetAll : IApiCall
	{
		public IEnumerable<Exoplanet> CallApi(NasaEpicHttpApi httpApi)
		{
			return httpApi.GetAll();
		}
	}

	internal class ApiCallGetExoplanetsByFilter : IApiCall
	{
		private readonly ExoplanetFields _exoplanetField;
		private readonly Comparison _comparison;
		private readonly object _value;

		public ApiCallGetExoplanetsByFilter(ExoplanetFields exoplanetField, Comparison comparison, object value)
		{
			_exoplanetField = exoplanetField;
			_comparison = comparison;
			_value = value;
		}

		public IEnumerable<Exoplanet> CallApi(NasaEpicHttpApi httpApi)
		{
			return httpApi.GetExoplanetsByFilter(_exoplanetField, _comparison, _value);
		}
	}

	internal class ApiCallGetExoplanetsByName : IApiCall
	{
		private readonly string _name;

		public ApiCallGetExoplanetsByName(string name)
		{
			_name = name;
		}

		public IEnumerable<Exoplanet> CallApi(NasaEpicHttpApi httpApi)
		{
			return httpApi.GetExoplanetsByName(_name);
		}
	}

	internal class NasaApiClassesWithErrorHandling : INasaEpicApi
	{
		private readonly NasaEpicHttpApi _httpApi;

		public event Action<string> CallSuccess;
		public event Action<string> CallFailed;

		public NasaApiClassesWithErrorHandling(int type)
		{
			_httpApi = new NasaEpicHttpApi(type);
		}

		public IEnumerable<Exoplanet> VratExoplanety()
		{
			var api = new ApiCallGetAll();
			return Call(api);
		}

		public IEnumerable<Exoplanet> VratExoplanety(ExoplanetFields exoplanetField, Comparison comparison, object value)
		{
			var api = new ApiCallGetExoplanetsByFilter(exoplanetField, comparison, value);
			return Call(api);
		}

		public IEnumerable<Exoplanet> VratExoplanety(string name)
		{
			var api = new ApiCallGetExoplanetsByName(name);
			return Call(api);
		}

		private IEnumerable<Exoplanet> Call(IApiCall apiCall)
		{
			var retries = 5;
			var timeouts = 0;
			Exception lastException = null;
			while (retries > 0)
			{
				try
				{
					return apiCall.CallApi(_httpApi);
				}
				catch (TimeoutException e)
				{
					Console.WriteLine("Increasing timeout");
					_httpApi.Timeout = _httpApi.Timeout.Add(TimeSpan.FromMinutes(1));
					lastException = new TimeoutException($"Timeouted for {timeouts} time!", e);
					timeouts++;
				}
				catch (Exception e)
				{
					lastException = e;
					Console.WriteLine(e.Message);
				}

				retries--;
			}

			throw lastException;
		}
	}
}