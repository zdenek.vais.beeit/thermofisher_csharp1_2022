﻿using System;
using System.Collections.Generic;
using BeeIt.Functory.Api;
using BeeIt.Functory.Api.Enums;
using BeeIt.Functory.Api.Models;

namespace BeeIt.Functory.Func.Wrappers
{
	internal class NasaApiWithErrorHandling : INasaEpicApi
	{
		private readonly NasaEpicHttpApi _httpApi;

		public event Action<string> CallSuccess;
		public event Action<string> CallFailed;

		public NasaApiWithErrorHandling(int type)
		{
			_httpApi = new NasaEpicHttpApi(type);
		}

		public IEnumerable<Exoplanet> VratExoplanety()
		{
			var retries = 5;
			var timeouts = 0;
			Exception lastException = null;
			while (retries > 0)
			{
				try
				{
					return _httpApi.GetAll();
				}
				catch (TimeoutException e)
				{
					Console.WriteLine("Increasing timeout");
					_httpApi.Timeout = _httpApi.Timeout.Add(TimeSpan.FromMinutes(1));
					lastException = new TimeoutException($"Timeouted for {timeouts} time!", e);
					timeouts++;
				}
				catch (Exception e)
				{
					lastException = e;
					Console.WriteLine(e.Message);
				}

				retries--;
			}

			throw lastException;
		}

		public IEnumerable<Exoplanet> VratExoplanety(ExoplanetFields exoplanetField, Comparison comparison, object value)
		{
			var retries = 5;
			var timeouts = 0;
			Exception lastException = null;
			while (retries > 0)
			{
				try
				{
					return _httpApi.GetExoplanetsByFilter(exoplanetField, comparison, value);
				}
				catch (TimeoutException e)
				{
					Console.WriteLine("Increasing timeout");
					_httpApi.Timeout = _httpApi.Timeout.Add(TimeSpan.FromMinutes(1));
					lastException = new TimeoutException($"Timeouted for {timeouts} time!", e);
					timeouts++;
				}
				catch (Exception e)
				{
					lastException = e;
					Console.WriteLine(e.Message);
				}

				retries--;
			}

			throw lastException;
		}

		public IEnumerable<Exoplanet> VratExoplanety(string name)
		{
			var retries = 5;
			var timeouts = 0;
			Exception lastException = null;
			while (retries > 0)
			{
				try
				{
					return _httpApi.GetExoplanetsByName(name);
				}
				catch (TimeoutException e)
				{
					Console.WriteLine("Increasing timeout");
					_httpApi.Timeout = _httpApi.Timeout.Add(TimeSpan.FromMinutes(1));
					lastException = new TimeoutException($"Timeouted for {timeouts} time!", e);
					timeouts++;
				}
				catch (Exception e)
				{
					lastException = e;
					Console.WriteLine(e.Message);
				}

				retries--;
			}

			throw lastException;
		}
	}
}