﻿using System;
using BeeIt.Functory.Api.Enums;
using BeeIt.Functory.Api.Models;
using BeeIt.Functory.Func.Wrappers;
using BeeIt.Functory.LazyLinq;
using BeeIt.Functory.Recursion;

namespace BeeIt.Functory.Func
{
	class Program
	{
		static void Main(string[] args)
		{
			//LazyLoading();
			CommandFunc();
			//Recursions();
			//Closure();

			Console.ReadKey();
		}

		private static void LazyLoading()
		{
			// Lazy LINQ
			new LazyLinqRun().Execute();
		}

		private static void CommandFunc()
		{
			// Functors
			// Typ chovani:
			//  0 - Zadne chyby
			//  1 - Prvni call je TimeoutException, druhy ok
			//  2 - Prvni 3 cally jsou TimeoutException, 4. ok
			//  3 - Prvni chyba je UnknownException, druhy ok
			var type = 0;

			// Wrappery
			// 1. Jednoduche volani bez osetrovani chyb
			var api = new NasaApiSimple(type);
			// 2. Jednoduche volani s osetrovanim chyb
			//var api = new NasaApiWithErrorHandling(type);
			// 3. Znovupouziti volani s pouzitim trid
			//var api = new NasaApiClassesWithErrorHandling(type);
			// 4. Znovupouziti volani s pouzitim functoru
			//var api = new NasaApiClassesWithFuncHandling(type);

			// Eventy pro vypis upsesnych a neuspesnych volani
			api.CallSuccess += (msg) => Print(msg, true);
			api.CallFailed += (msg) => Print(msg, false);

			// Volani
			var exoplanets1 = api.VratExoplanety();
			//var exoplanets2 = api.VratExoplanety(ExoplanetFields.DistanceInParsecs, Comparison.LESS_THAN, 3);
			//var exoplanets3 = api.VratExoplanety("Proxima");
		}

		private static void Recursions()
		{
			// Recursions
			var factorial = RecursionMethods.Factorial(5);
			var palindrome1 = RecursionMethods.IsPalindrome("abba");
			var palindrome2 = RecursionMethods.IsPalindrome("abc");
			var reversedString = RecursionMethods.ReverseString("abcdefghi");
		}

		private static void Closure()
		{
			// Closure
			new Closure().Execute();
		}

		private static void Print(string msg, bool success)
		{
			var text = success ? "Podarilo" : "Nepodarilo";
			Console.WriteLine($"{text} se: {msg}");
		}
	}
}
