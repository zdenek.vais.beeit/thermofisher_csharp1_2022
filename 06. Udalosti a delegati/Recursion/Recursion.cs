﻿namespace BeeIt.Functory.Recursion
{
	public static class RecursionMethods
	{
		// Factorial: n! = n*(n-1)*(n-2)*...*1
		public static int Factorial(int n)
		{
			if (n == 0) return 0;
			if (n == 1) return 1;

			return n * Factorial(n - 1);
		}

		public static bool IsPalindrome(string text)
		{
			if (text == null) return false; // Questionable
			if (text.Length == 0) return true;

			return IsPalindrome(text, 0, text.Length - 1);
		}

		private static bool IsPalindrome(string text, int startIndex, int endIndex)
		{
			if (startIndex == endIndex) return true;
			if (text[startIndex] != text[endIndex]) return false;

			if (startIndex < endIndex)
			{
				return IsPalindrome(text, startIndex + 1, endIndex - 1);
			}

			return true;
		}

		public static string ReverseString(string text)
		{
			if (text == null) return null;
			if (text.Length == 0) return string.Empty;

			string subString = text.Substring(0, text.Length - 1);
			return text[text.Length - 1] + ReverseString(subString); // Not effective
		}
	}
}
