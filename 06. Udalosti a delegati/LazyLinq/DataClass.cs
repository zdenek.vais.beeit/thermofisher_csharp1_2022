﻿using System.Threading;

namespace BeeIt.Functory.LazyLinq
{
	internal class DataClass
	{
		private double _value;
		public double Value
		{
			get
			{
				Thread.Sleep(5);
				return _value;
			}
			set
			{
				_value = value;
			}
		}
	}
}
