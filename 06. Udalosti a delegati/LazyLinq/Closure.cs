﻿using System;

namespace BeeIt.Functory.LazyLinq
{
	public class Closure
	{
		public void Execute()
		{
			string name = "";

			name = "Zdenek";
			Action print1 = () => Console.WriteLine(name);
			name = "Peter";
			Action print2 = () => Console.WriteLine(name);
			name = "Marian";
			Action print3 = () => Console.WriteLine(name);

			print1();
			print2();
			print3();
		}
	}
}
