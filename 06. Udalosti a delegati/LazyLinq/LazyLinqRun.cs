﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace BeeIt.Functory.LazyLinq
{
	public class LazyLinqRun
	{
		private const int SIZE = 1000;

		public void Execute()
		{
			var data = InitializeArray();

			Console.WriteLine("Copying...");
			var outputData1 = CopyNoFilter(data);
			var outputData2 = CopyFilteredClassic(data);
			var outputData3 = CopyFilteredLinq(data);

			Console.WriteLine();
			Console.WriteLine("Printing...");
			Print(outputData1, "no filter");
			Print(outputData2, "filter classic");
			Print(outputData3, "filter linq");
		}

		private IEnumerable<DataClass> InitializeArray()
		{
			var list = new List<DataClass>();
			for (var i = 0; i < SIZE; i++)
			{
				list.Add(new DataClass()
				{
					Value = i,
				});
			}
			return list;
		}

		private IEnumerable<double> CopyNoFilter(IEnumerable<DataClass> data)
		{
			var sw = new Stopwatch();
			sw.Start();

			var outputList = new List<double>();
			foreach (var item in data)
			{
				outputList.Add(item.Value);
			}

			sw.Stop();
			Console.WriteLine($"{nameof(CopyNoFilter)}:\t\t{sw.Elapsed}");

			return outputList;
		}

		private IEnumerable<double> CopyFilteredClassic(IEnumerable<DataClass> data)
		{
			var sw = new Stopwatch();
			sw.Start();

			var list1 = new List<DataClass>();
			foreach (var item in data)
			{
				if (item.Value < 500)
				{
					list1.Add(item);
				}
			}

			var list2 = new List<DataClass>();
			foreach (var item in list1)
			{
				if (item.Value < 200)
				{
					list2.Add(item);
				}
			}

			var list3 = new List<DataClass>();
			foreach (var item in list2)
			{
				if (item.Value < 100)
				{
					list3.Add(item);
				}
			}

			var outputList = new List<double>();
			foreach (var item in list3)
			{
				outputList.Add(item.Value);
			}

			sw.Stop();
			Console.WriteLine($"{nameof(CopyFilteredClassic)}:\t{sw.Elapsed}");

			return outputList;
		}

		private IEnumerable<double> CopyFilteredLinq(IEnumerable<DataClass> data)
		{
			var sw = new Stopwatch();
			sw.Start();

			var result = data
				.Where(x => x.Value < 500)
				.Where(x => x.Value < 200)
				.Where(x => x.Value < 100)
				.Select(x => x.Value);

			sw.Stop();
			Console.WriteLine($"{nameof(CopyFilteredLinq)}:\t{sw.Elapsed}");

			return result;
		}

		private void Print(IEnumerable<double> data, string type)
		{
			var sw = new Stopwatch();
			sw.Start();
			var concatenate = "";

			foreach (var item in data)
			{
				concatenate += item;
			}

			sw.Stop();
			Console.WriteLine($"{nameof(Print)}-{type}: \t{sw.Elapsed}");
		}
	}
}
