﻿namespace BeeIT.IO.App.Basics;

internal class BackupFiles
{
    public static void BackupFilesWithOverride(string sourceDirPath, string destinationDirPath)
    {
        // Vytvoreni cilove slozky
        Directory.CreateDirectory(destinationDirPath);

        // Kopirovani slozek
        var subDirectoryPathes = Directory.GetDirectories(sourceDirPath, "*", SearchOption.AllDirectories);
        foreach (var sourceSubDirPath in subDirectoryPathes)
        {
            var relativePath = Path.GetRelativePath(sourceDirPath, sourceSubDirPath);
            var destinationSubDirPath = Path.Combine(destinationDirPath, relativePath);

            Console.WriteLine($"Creating dir: {destinationSubDirPath}");
            Directory.CreateDirectory(destinationSubDirPath);
        }

        // Kopirovani souboru
        var sourceFilePathes = Directory.GetFiles(sourceDirPath, "*", SearchOption.AllDirectories);
        foreach (var sourceFilePath in sourceFilePathes)
        {
            var relativePath = Path.GetRelativePath(sourceDirPath, sourceFilePath);
            var destinationFilePath = Path.Combine(destinationDirPath, relativePath);

            Console.WriteLine($"Copying: {sourceFilePath} -> {destinationFilePath}");
            File.Copy(sourceFilePath, destinationFilePath, true);
        }
    }
}