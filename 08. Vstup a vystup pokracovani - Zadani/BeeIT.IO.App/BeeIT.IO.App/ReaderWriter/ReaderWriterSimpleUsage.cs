﻿using System.Text;

namespace BeeIT.IO.App.ReaderWriter;

internal class ReaderWriterSimpleUsage
{
    public static void ReaderExample(string filePath)
    {
        using var fileStream = File.OpenRead(filePath);
        using var reader = new StreamReader(fileStream, Encoding.UTF8);
        string line = null;
        while ((line = reader.ReadLine()) != null)
        {
            Console.WriteLine(line);
        }
    }


    public static void ConvertUtf8ToAscii(string existingFile, string newFile)
    {
        // TODO: Pomoci tridy StreamReader a StreamWriter prevedte soubor z UTF-8 do Unicode

    }
}
