﻿namespace BeeIT.IO.App.Streams;

internal class SimpleStreamUsage
{
    public static long GetEvenByteCount(string filePath)
    {
        using var fileStream = File.OpenRead(filePath);

        int readByte = 0;
        long evenBytes = 0;
        while ((readByte = fileStream.ReadByte()) > -1)
        {
            if (readByte % 2 == 0)
            {
                evenBytes++;
            }
        }

        return evenBytes;
    }

    public static long GetHash(string filePath)
    {
        int hash = 0; 

        // TODO: Doplnte kod na vypocet jednoducheho hashe
        // Hash bude dan jako XOR (operator ^) s kazdym 5. bytem
        // a jiz vypocitanym hashem

        return hash;
    }
}

