﻿namespace BeeIT.IO.App.Exceptions;

internal class ExceptionExamples
{
    private static void CatchingExceptions()
    {
        int result = 0;
        try
        {
            Console.Write("Input divident: ");
            int divident = int.Parse(Console.ReadLine());

            Console.Write("Input factor: ");
            int factor = int.Parse(Console.ReadLine());

            result = divident / factor;
        }
        catch (DirectoryNotFoundException ex)
        {
            Console.WriteLine($"Wrong number: {ex}");
        }
        catch (IOException ex)
        {
            Console.WriteLine($"Division by zero: {ex.Message}");
        }
        catch(Exception ex)
        {
            Console.WriteLine($"Unknown exception.");
        }
        finally
        {
            Console.WriteLine(result);
        }
    }
}
