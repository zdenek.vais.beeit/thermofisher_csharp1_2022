﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace CleanCode.Refactoring;

public class PriceCalculator2
{
    private const int YearLimitForLoyaltyDiscount = 5;
    private static readonly IReadOnlyDictionary<AccountStatus, decimal> relativeDiscountPerAccountStatus = new Dictionary<AccountStatus, decimal>
    {
        { AccountStatus.NotRegistered, 0 },
        { AccountStatus.SimpleCustomer, 0.1m },
        { AccountStatus.ValuableCustomer, 0.3m },
        { AccountStatus.MostValuableCustomer, 0.5m }
    }.AsReadOnly();

    private readonly AccountStatus accountStatus;
    private readonly int timeOfHavingAccountInYears;
    public PriceCalculator2(AccountStatus accountStatus, int timeOfHavingAccountInYears)
    {
        if (!relativeDiscountPerAccountStatus.ContainsKey(accountStatus))
        {
            throw new InvalidEnumArgumentException($"Unsupported account status '{accountStatus}' to calculate discounts.");
        }

        if (timeOfHavingAccountInYears < 0)
        {
            throw new ArgumentOutOfRangeException(nameof(timeOfHavingAccountInYears), "Account age must be a positive number.");
        }

        this.accountStatus = accountStatus;
        this.timeOfHavingAccountInYears = timeOfHavingAccountInYears;
    }

    public decimal GetPriceAfterDiscount(decimal originalPrice)
    {
        if (originalPrice <= 0)
        {
            throw new ArgumentOutOfRangeException(nameof(originalPrice), "Price must be a positive number.");
        }
        
        var priceAfterDiscount = GetPriceAfterRoyaltyDiscount(originalPrice, timeOfHavingAccountInYears);
        return GetPriceAfterAccountStatusDiscount(priceAfterDiscount, accountStatus);
    }

    private static decimal GetPriceAfterRoyaltyDiscount(decimal originalPrice, int timeOfHavingAccountInYears)
    {
        int yearsToCalculateLoyaltyDiscount = Math.Min(timeOfHavingAccountInYears, YearLimitForLoyaltyDiscount);
        var relativeRoyaltyDiscount = yearsToCalculateLoyaltyDiscount / 100;

        return originalPrice * relativeRoyaltyDiscount;
    }

    private static decimal GetPriceAfterAccountStatusDiscount(decimal originalPrice, AccountStatus accountStatus)
    {
        decimal relativeAccountStatusDiscount = relativeDiscountPerAccountStatus[accountStatus];
        return relativeAccountStatusDiscount * originalPrice;
    }
}
