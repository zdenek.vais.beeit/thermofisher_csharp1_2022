﻿namespace BeeIT.Collections.App._04List;
    internal class ListDemonstration
{
    public static void ZakladniOperaceAMetody()
    {
        List<int> seznam = new List<int>();
        int vysledek = 0;

        // foreach vyuziva iterator IEnumerator pro prochazeni vsech prvku kolekce
        foreach (int cislo in seznam)
        {
            vysledek += cislo;
        }

        // pridavani do kolekce
        seznam.Add(vysledek);

        // odstranovani z kolekce
        bool bylOdstranen = seznam.Remove(vysledek);

        // test vyskytu v kolekci
        bool existujeVSeznamu = seznam.Contains(vysledek);

        // pocet prvku v kolekci
        int pocetPrvku = seznam.Count();
    }
}

