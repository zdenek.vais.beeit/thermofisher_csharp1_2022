﻿namespace BeeIT.Collections.App._01Arrays;

internal class JaggedArrayDemonstration
{
    public static void Show()
    {
        // Nekdy se jim rika zubata (jagged) pole
        bool[][] kinosala = new bool[10][];             // 10 rad, ale kazda rada muze mit jiny pocet sedadel (vlastni pole)

        // Vyhodou je moznost definovat vlastni velkost kazdeho "vnitrniho" pole
        // nevyhodou je nutnost inicializace  techto "vnitrnich" poli
        kinosala[0] = new bool[8];                      // prvni rada ma 8 sedadel
        kinosala[1] = new bool[10];                     // druha rada ma 10 sedadel
        kinosala[2] = new bool[12];                     // treti rada ma 12 sedadel, atd...

        kinosala[2][6] = true;

        bool jeSedadloObsazeno = kinosala[2][6];        // je sedadlo c. 7 v treti rade obsazeno == true

        // zkracena inicializace
        bool[][][] kinosaly = new bool[2][][]           // 2 kinosaly, kazda s ruznym poctem rad a sedadel
        {
                new bool[3][]                               // prvni kinosal ma 3 rady
                {
                    new bool[] { true, false, true },       // prvni rada ma 3 sedadla
                    new bool[] { false, true, true, false}, // druha a treti rada maji 4 sedadla
                    new bool[] { false, true, true, true}
                },
                new bool[4][]                               // druhy kinosal ma 4 rady
                {
                    new bool[] { true, false, true },       // prvni a druha rada maji 3 sedadla
                    new bool[] { false, true, true},
                    new bool[] { false, true, true, true},  // treti a ctvrta rada maji 4 sedadla
                    new bool[] { true, true, true, true }
                }
        };
    }
}

