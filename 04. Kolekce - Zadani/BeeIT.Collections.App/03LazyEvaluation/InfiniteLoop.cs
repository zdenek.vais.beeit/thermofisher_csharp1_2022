﻿namespace BeeIT.Collections.App._03LazyEvaluation;

public static class InfiniteLoop
{
    private static IEnumerable<int> GetNaturalNumbers()
    {
        int i = 0;
        while (true)
        {
            yield return ++i;
        }
    }
}
