﻿namespace BeeIT.Collections.App._05Dictionaries;

internal class DictionaryDemonstration
{
    public static void Show()
    {
        // vytvoreni noveho slovniku a pridani do slovniku
        var slovnik = new Dictionary<int, string>();
        slovnik.Add(1, "jedna");

        // KeyValuePair - slovnik je vlastne List objektov typu KeyValuePair
        var keypair = new KeyValuePair<int, string>(2, "dva");
        slovnik.Add(keypair.Key, keypair.Value);

        // odstraneni ze slovniku - staci jenom klic
        var byloOdstraneno = slovnik.Remove(2);

        // test na pritomnost klice/hodnoty
        var test = slovnik.ContainsKey(2);
        test = slovnik.ContainsValue("dva");

        // vyber hodnoty ze slovniku
        var hodnota = slovnik[2];
        if (slovnik.TryGetValue(2, out hodnota))
        {
            // udelej neco jestli existuje klic 2
            // v promenne hodnota bude ulozen string "dva"
        }

        foreach (var cislo in slovnik.Keys)
        {
            Console.WriteLine($"Cislo {cislo} je slovem: {slovnik[cislo]}");
        }
    }
}