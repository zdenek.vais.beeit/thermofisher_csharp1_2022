﻿namespace BeeIT.Collections.App._05Dictionaries;

public class User
{
    public string Name { get; set; }
    public int Age { get; set; }
}
internal class HashingProblem
{
    // Co se stane?
    public static void Show()
    {
        var user1 = new User
        {
            Age = 18,
            Name = "Zdenek"
        };

        var user2 = new User
        {
            Age = 18,
            Name = "Zdenek"
        };

        var dictionary = new Dictionary<User, string>();
        dictionary.Add(user1, "Motto");
        dictionary.Add(user2, "Motto");
    }
}

