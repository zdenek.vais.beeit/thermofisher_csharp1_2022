﻿namespace BeeIT.Collections.App._06Set;

internal class SetDemonstration
{
    public static void Show()
    {
        // HashSet
        var set = new HashSet<string>();
        set.Add("test");
        set.Add("dalsi");
        set.Contains("test");

        // je podmnozinou
        set.IsSubsetOf(new string[1] { "dalsi" });
        // je nadmnozinou
        set.IsSupersetOf(new string[1] { "dalsi" });
        // spoji dve mnoziny
        set.UnionWith(new string[2] { "jeste", "jeden" });
    }
}