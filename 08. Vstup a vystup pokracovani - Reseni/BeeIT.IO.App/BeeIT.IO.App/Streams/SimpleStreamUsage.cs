﻿namespace BeeIT.IO.App.Streams;

internal class SimpleStreamUsage
{
    public static long GetEvenByteCount(string filePath)
    {
        using var fileStream = File.OpenRead(filePath);

        int readByte = 0;
        long evenBytes = 0;
        while ((readByte = fileStream.ReadByte()) > -1)
        {
            if (readByte % 2 == 0)
            {
                evenBytes++;
            }
        }

        return evenBytes;
    }

    public static long GetChecksum(string filePath)
    {
        using var fileStream = File.OpenRead(filePath);

        int counter = 0;
        int readByte = 0;
        int checksum = 0;
        while ((readByte = fileStream.ReadByte()) > -1)
        {
            if (++counter % 5 == 0)
            {
                checksum ^= readByte;
            }
        }

        return checksum;
    }
}

