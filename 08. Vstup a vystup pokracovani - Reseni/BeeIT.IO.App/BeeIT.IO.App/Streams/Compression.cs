﻿using System.IO.Compression;

namespace BeeIT.IO.App.Streams;

public class Compression
{
    public static bool CompressFileWithoutBuffer(string sourceFile, string destinationFile)
    {
        if (!File.Exists(sourceFile))
        {
            return false;
        }

        var allBytes = File.ReadAllBytes(sourceFile);

        using var destinationStream = File.Create(destinationFile);
        using var compressionStream = new GZipStream(destinationStream, CompressionLevel.SmallestSize);
        compressionStream.Write(allBytes, 0, allBytes.Length);

        return true;
    }

    public static void CompressFileWithBuffer(string sourceFile, string destinationFile, int bufferSize = 1024)
    {
        int bytesInBuffer = -1;
        var buffer = new byte[bufferSize];

        using var sourceStream = File.OpenRead(sourceFile);
        using var destinationStream = File.Create(destinationFile);
        using var compressionStream = new DeflateStream(destinationStream, CompressionLevel.SmallestSize);
        while ((bytesInBuffer = sourceStream.Read(buffer, 0, buffer.Length)) > 0)
        {
            compressionStream.Write(buffer, 0, bytesInBuffer);
        }
    }
}
