﻿using System.Text;

namespace BeeIT.IO.App.ReaderWriter;

internal class ReaderWriterSimpleUsage
{
    public static void ReaderExample(string filePath)
    {
        using var fileStream = File.OpenRead(filePath);
        using var reader = new StreamReader(fileStream, Encoding.UTF8);
        string line = null;
        while ((line = reader.ReadLine()) != null)
        {
            Console.WriteLine(line);
        }
    }


    public static void WriterExample(string existingFile, string newFile)
    {
        using var reader = new StreamReader(existingFile, Encoding.UTF8);
        using var writeStream = File.Create(newFile);
        using var writer = new StreamWriter(writeStream, Encoding.ASCII);
        var result = reader.ReadToEnd();
        writer.Write(result);
    }
}
