﻿namespace BeeIT.IO.App.Basics;

public class IOBasicsExample
{
    private const string exampleFolder = @"..\..\..\..\Examples";
    private const string source = @"..\..\..\..\Examples\Source";
    private const string target = @"..\..\..\..\Examples\Target";


    public static void PathExamples()
    {
        // Bezpecne skladani cest
        var finalPath = Path.Combine(@"C:\directoryA", "directoryB");
        Console.WriteLine(finalPath);

        // Separator slozek nezavisly na platforme
        var dirSeparator = Path.DirectorySeparatorChar;

        // Obsahuje pocatek cesty (disk)?
        var isRooted = Path.IsPathRooted(@"..\dir\directory\test.txt");
        Console.WriteLine(isRooted);

        // Relativni cesta
        var relativePath = Path.GetRelativePath(@"C:\directoryA\", @"C:\directoryA\directoryB\file.txt");
        Console.WriteLine(relativePath);
    }

    public static void DirectoryExamples()
    {
        // vytvorime si zdrojovou slozku
        if (!Directory.Exists(source))
        {
            Directory.CreateDirectory(source);
        }

        Console.WriteLine("Seznam pod-slozek:");
        foreach (var folder in Directory.GetDirectories(exampleFolder))
        {
            Console.WriteLine(folder);
        }

        Console.WriteLine("Seznam souboru:");
        foreach (var file in Directory.EnumerateFiles(exampleFolder))
        {
            Console.WriteLine(file);
        }

        Directory.Delete(source);
        Directory.Delete(target);
    }

    public static void FileExample()
    {
        if (File.Exists(Path.Combine(exampleFolder, "textak.txt")))
        {
            // skopirujeme textak do source
            File.Copy(Path.Combine(exampleFolder, "textak.txt"), Path.Combine(source, "textak.txt"));
        }

        // presuneme textak do target
        File.Move(Path.Combine(source, "textak.txt"), Path.Combine(target, "textak.txt"));

        // vymazeme textak
        File.Delete(Path.Combine(target, "textak.txt"));
    }
}

