﻿using System;
using System.Collections.Generic;
using System.Linq;
using BeeIt.Linq.Comparer;

namespace BeeIt.Linq
{
	public class CviceniUloha : ICviceni
	{
		// 1. Počet obcí
		public int? PocetObci(IEnumerable<Obec> obce)
		{
			return null;
		}

		// 2. Součet obyvatel všech obcí
		public int? SoucetObyvatel(IEnumerable<int> pocetObyvatelObci)
		{
			return null;
		}

		// 3. Průměrný počet obyvatel na jednu obec
		public double? PrumerObyvatelNaObec(IEnumerable<int> pocetObyvatelObci)
		{
			return null;
		}

		// 4. Největší počet obyvatel v jedné obci
		public int? NejvetsiPocetObyvatelVJedneObci(IEnumerable<int> pocetObyvatelObci)
		{
			return null;
		}

		// 5. Nejmenší počet obyvatel v jedné obci
		public int? NejmensiPocetObyvatelVJedneObci(IEnumerable<int> pocetObyvatelObci)
		{
			return null;
		}

		// 6. Pouze obce s více než 100000 obyvateli
		public IEnumerable<Obec> ObceFiltr1(IEnumerable<Obec> obce)
		{
			return null;
		}

		// 7. Pouze obce v Jihomoravském kraji
		public IEnumerable<Obec> ObceFiltr2(IEnumerable<Obec> obce)
		{
			return null;
		}

		// 8. Pouze obce, která mají CzechPoint
		public IEnumerable<Obec> ObceFiltr3(IEnumerable<Obec> obce)
		{
			return null;
		}

		// 9. Pouze města s matričním úřadem
		public IEnumerable<Obec> ObceFiltr4(IEnumerable<Obec> obce)
		{
			return null;
		}

		// 10. Pouze obce, která mají regionální pracovniště a zároveň pracovní úřad
		public IEnumerable<Obec> ObceFiltr5(IEnumerable<Obec> obce)
		{
			return null;
		}

		// 11. Pouze obce v kategorii městys nebo vesnice
		public IEnumerable<Obec> ObceFiltr6(IEnumerable<Obec> obce)
		{
			return null;
		}

		// 12. Pouze obce se statusem město nebo statutární město
		public IEnumerable<Obec> ObceFiltr7(IEnumerable<Obec> obce)
		{
			return null;
		}

		// 13. Vesnice v Jihomoravském kraji
		public IEnumerable<Obec> ObceFiltr8(IEnumerable<Obec> obce)
		{
			return null;
		}

		// 14. Seřazené obce podle počtu obyvatel vzestupně
		public IEnumerable<Obec> Serazeni1(IEnumerable<Obec> obce)
		{
			return null;
		}

		// 15. Seřazené obce podle počtu obyvatel sestupně
		public IEnumerable<Obec> Serazeni2(IEnumerable<Obec> obce)
		{
			return null;
		}

		// 16. Pouze názvy obcí
		public IEnumerable<string> SelectNazev(IEnumerable<Obec> obce)
		{
			return null;
		}

		// 17. Pouze počty obyvatel
		public IEnumerable<int> SelectPocetObyvatel(IEnumerable<Obec> obce)
		{
			return null;
		}

		// 18. Pouze hodnoty základní působnosti
		public IEnumerable<string> SelectZakladniPusobnost(IEnumerable<Obec> obce)
		{
			return null;
		}

		// 19. Existuje obec, které má v názvu "x"?
		public bool? JakykolivObsajujeX(IEnumerable<Obec> obce)
		{
			return null;
		}

		// 20. Mají všechny obce v názvu "a"?
		public bool? VsichniObsahujiA(IEnumerable<Obec> obce)
		{
			return null;
		}

		// 21. První město v pořadí
		public Obec Prvni(IEnumerable<Obec> obce)
		{
			return null;
		}

		// 22. První nebo žádná obec
		public Obec PrvniNeboZadny(IEnumerable<Obec> obce)
		{
			return null;
		}

		// 23. Počet obcí s více než 10000 obyvateli
		public int? PocetObciSObyvateli(IEnumerable<Obec> obce)
		{
			return null;
		}

		// 24. Počet obcí v Pardubickém kraji
		public int? PocetObciVPardubickemKraji(IEnumerable<Obec> obce)
		{
			return null;
		}

		//==============================================================

		// 25. Počet obyvatel v Zlínském kraji
		public int? PocetObyvatelVZlínskémKraji(IEnumerable<Obec> obce)
		{
			return null;
		}

		// 26. Průměrný počet obyvatel v Moravskoslezském kraji na jednu obec
		public double? PrumernyPocetObyvatelVMoravskoslezskemKraji(IEnumerable<Obec> obce)
		{
			return null;
		}

		// 27. Prvních 5 nejlidnatnějších měst v Jihomoravském kraji
		public IEnumerable<Obec> NejlidnatejsiObcevJihomoravskemKraji(IEnumerable<Obec> obce)
		{
			return null;
		}

		// 28. Počet obcí s pracovním úradem v Středočeském kraji
		public int? PocetObciSPracovnimUrademVStredoceskemKraji(IEnumerable<Obec> obce)
		{
			return null;
		}

		//==============================================================

		// 29. Kraje, které mají více než 1 mln obyvatel
		public IEnumerable<Kraj> KrajeSViceNez1MlnObyvateli(IEnumerable<Obec> obce)
		{
			return null;
		}

		// 30. Nejvíce obyvatel v každém kraji
		public Dictionary<Kraj, int> NejviceObyvatelVKazdemKraji(IEnumerable<Obec> obce)
		{
			return null;
		}
	}
}
