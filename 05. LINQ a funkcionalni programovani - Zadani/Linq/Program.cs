﻿using BeeIt.Linq.Comparer;
using System;
using System.Text;

namespace BeeIt.Linq
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.OutputEncoding = Encoding.UTF8;

			var loader = new DataLoader();
			var mesta = loader.NactiObce();

			var cviceniUloha = new CviceniUloha();
			var cviceniReseni = new CviceniReseni();
			var comparer = new DataComparer(cviceniUloha, cviceniReseni);
			comparer.Compare(mesta);

			Console.ReadKey();
		}
	}
}
