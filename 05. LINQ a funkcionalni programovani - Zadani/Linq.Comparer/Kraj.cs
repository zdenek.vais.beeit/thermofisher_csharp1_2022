﻿namespace BeeIt.Linq.Comparer
{
	public enum Kraj
	{
		Karlovarsky,
		Jihocesky,
		Stredocesky,
		Jihomoravsky,
		Kralovehradecky,
		Moravskoslezsky,
		Pardubicky,
		Liberecky,
		Olomoucky,
		Vysocina,
		Ustecky,
		Zlinsky,
		Plzensky,
		Praha,
	}
}
