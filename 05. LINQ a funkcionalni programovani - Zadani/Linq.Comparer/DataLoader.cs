﻿using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace BeeIt.Linq.Comparer
{
	public class DataLoader
	{
		private readonly string PATH = Directory.GetCurrentDirectory() + "/../../Data/Databaze_obci_1-1-2014.csv";

		public IEnumerable<Obec> NactiObce()
		{
			var dataLines = File.ReadAllLines(PATH, System.Text.Encoding.UTF8);
			var mesta = new List<Obec>();

			foreach (var line in dataLines.Skip(1))
			{
				var parts = line.Split(';');

				mesta.Add(new Obec
				{
					Index = int.Parse(parts[0]),
					Nazev = parts[1],
					PocetObyvatel = int.Parse(parts[2]),
					Id = int.Parse(parts[3]),
					IdentifikacniCisloOrganizace = int.Parse(parts[4]),
					Kategorie = ParseKategorie(parts[5]),
					Okres = parts[6],
					OkresId = int.Parse(parts[7]),
					Kraj = ParseKraj(parts[8]),
					ZakladniPusobnost = parts[9],
					MaMatricniUrad = int.Parse(parts[21]) == 1,
					MaPracovniUrad = int.Parse(parts[23]) == 1,
					MaRegionalniPracoviste = int.Parse(parts[24]) == 1,
					MaVidimacniUrad = int.Parse(parts[28]) == 1,
					MaCzechPoint = int.Parse(parts[29]) == 1,
				});
			}

			return mesta;
		}

		private Kategorie ParseKategorie(string kategorie)
		{
			switch (kategorie)
			{
				case "město":
					return Kategorie.Mesto;
				case "obec":
					return Kategorie.Vesnice;
				case "městys":
					return Kategorie.Mestys;
				case "vojenský újezd":
					return Kategorie.VojenskyUjezd;
				case "statutární město":
					return Kategorie.StatutarniMesto;
				case "hlavní město":
					return Kategorie.HlavniMesto;
				default:
					return Kategorie.Mesto;
			}
		}

		private Kraj ParseKraj(string kraj)
		{
			switch (kraj)
			{
				case "Karlovarský kraj":
					return Kraj.Karlovarsky;
				case "Jihočeský kraj":
					return Kraj.Jihocesky;
				case "Středočeský kraj":
					return Kraj.Stredocesky;
				case "Jihomoravský kraj":
					return Kraj.Jihomoravsky;
				case "Královéhradecký kraj":
					return Kraj.Kralovehradecky;
				case "Moravskoslezský kraj":
					return Kraj.Moravskoslezsky;
				case "Pardubický kraj":
					return Kraj.Pardubicky;
				case "Liberecký kraj":
					return Kraj.Liberecky;
				case "Olomoucký kraj":
					return Kraj.Olomoucky;
				case "Kraj Vysočina":
					return Kraj.Vysocina;
				case "Ústecký kraj":
					return Kraj.Ustecky;
				case "Zlínský kraj":
					return Kraj.Zlinsky;
				case "Plzeňský kraj":
					return Kraj.Plzensky;
				case "Praha":
					return Kraj.Praha;
				default:
					return Kraj.Karlovarsky;
			}
		}
	}
}
