﻿using System;

namespace BeeIT.UnitTesting.Shapes;

public class Circle
{
    private double _radius;

    public Circle(double radius)
    {
        Radius = radius;
    }

    public double Radius 
    {
        get => _radius;
        set
        {
            if (value <= 0)
            {
                throw new ArgumentOutOfRangeException($"{nameof(Radius)} cannot be less than zero!");
            }
            _radius = value;
        }
    }

    public double Diameter
    {
        get => Radius * 2;
        set => Radius = value / 2;
    }

    public double Area => Math.PI * Math.Pow(Diameter, 2);

    public double GetSectorArea(double thetaAngle)
    {
        if (thetaAngle <= 0 || thetaAngle > 360)
        {
            throw new ArgumentOutOfRangeException($"{nameof(thetaAngle)} must be between 0 and 360!");
        }

        return Area * thetaAngle / 360.0;
    }
}
