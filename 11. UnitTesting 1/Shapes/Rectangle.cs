﻿using System;

namespace BeeIT.UnitTesting.Shapes;

public class Rectangle
{
    private double _height;
    private double _length;

    public Rectangle(double height, double length)
    {
        Height = height;
        Length = length;
    }

    public double Height
    {
        get => _height; 
        set 
        {
            if (_height < 0)
            {
                throw new ArgumentOutOfRangeException($"{nameof(Height)} cannot be less than zero!");
            }
            _height = value;
        }
    }

    public double Length
    {
        get => _length;
        set
        {
            if (_length <= 0)
            {
                throw new ArgumentOutOfRangeException($"{nameof(Length)} cannot be less than zero!");
            }
            _length = value;
        }
    }

    public double Area => Height * Length;

    public double Diagonal => Math.Sqrt(Math.Pow(Height, 2) + Math.Pow(Length, 2));
}
