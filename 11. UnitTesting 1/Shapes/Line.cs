﻿using BeeIT.UnitTesting.Shapes.Common;
using System;

namespace BeeIT.UnitTesting.Shapes;

public class Line
{
    private Point _pointA;
    private Point _pointB;

    public Point PointA
    {
        get => _pointA;
        set
        {
            ThrowIfPointsOverlap(value, PointB);
            _pointA = value;
        }
    }

    public Point PointB
    {
        get => _pointB;
        set
        {
            ThrowIfPointsOverlap(PointA, value);
            _pointB = value;
        }
    }

    public double Angle
    {
        get
        {
            var line1 = new Line
            {
                PointA = Point.Create(0, 0),
                PointB = Point.Create(PointB.X - PointA.X, PointB.Y - PointA.Y),
            };
            var line2 = new Line
            {
                PointA = Point.Create(0, 0),
                PointB = Point.Create(PointB.X - PointA.X, 0),
            };
            return Math.Acos(line2.Distance / line1.Distance) / Math.PI * 180;
        }
    }

    public double Distance => PointA is { } && PointB is { } ? Math.Sqrt(
        Math.Pow(PointA.X - PointB.X, 2) +
        Math.Pow(PointA.Y - PointB.Y, 2)) : 0;

    private void ThrowIfPointsOverlap(Point a, Point b)
    {
        if (a is not null && b is not null && a.X == b.X && a.Y == b.Y)
        {
            throw new ArgumentException($"Both points are overlapping at [{a.X},{a.Y}]!");
        }
    }
}
