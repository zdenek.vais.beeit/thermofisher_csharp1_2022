﻿using System.Diagnostics;

namespace BeeIT.UnitTesting.Shapes.Common;

[DebuggerDisplay("[{X}, {Y}]")]
public class Point
{
    public Point(double x, double y)
    {
        X = x;
        Y = y;
    }

    public static Point Create(double x, double y)
    {
        return new Point(x, y);
    }

    public double X { get; set; }
    public double Y { get; set; }
}