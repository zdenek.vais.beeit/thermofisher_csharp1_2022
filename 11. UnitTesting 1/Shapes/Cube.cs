﻿using System;

namespace BeeIT.UnitTesting.Shapes;

public class Cube
{
    private double _length;

    public Cube(double length)
    {
        Length = length;
    }

    public double Length 
    {
        get => _length;
        set
        {
            if (_length <= 0)
            {
                throw new ArgumentOutOfRangeException($"{nameof(Length)} cannot be less than zero!");
            }
            _length = value;
        }
    }

    public double Volume => Math.Pow(Length, 3);

    public double Area => 6 * Math.Pow(Length, 2);

    public double Diagonal => Math.Pow(Length, 1.0 / 3.0);
}
