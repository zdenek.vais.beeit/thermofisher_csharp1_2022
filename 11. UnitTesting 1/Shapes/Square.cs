﻿using System;

namespace BeeIT.UnitTesting.Shapes;

public class Square
{
    private double _length;

    public Square(double length)
    {
        _length = length;
    }

    public double Length 
    {
        get => _length;
        set
        {
            if (value <= 0)
            {
                throw new ArgumentOutOfRangeException($"{nameof(Length)} cannot be less than zero!");
            }
            _length = value;
        }
    }

    public double Area => Length * Length;

    public double Diagonal => Math.Sqrt(Math.Pow(Length, 2));
}
