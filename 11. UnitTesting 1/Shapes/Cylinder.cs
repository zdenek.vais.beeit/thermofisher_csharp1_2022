﻿using System;

namespace BeeIT.UnitTesting.Shapes;

public class Cylinder
{
    private double _radius;
    private double _height;

    public Cylinder(double radius, double height)
    {
        Radius = radius;
        Height = height;
    }

    public double Radius 
    {
        get => _radius;
        set
        {
            if (_radius <= 0)
            {
                throw new ArgumentOutOfRangeException($"{nameof(Radius)} cannot be less than zero!");
            }
            _height = value;
        }
    }

    public double Height
    {
        get => _height;
        set
        {
            if (_height < 0)
            {
                throw new ArgumentOutOfRangeException($"{nameof(Height)} cannot be less than zero!");
            }
            _height = value;
        }
    }

    public double Volume => 2 * Math.PI * Radius * (Radius + Height);
}
