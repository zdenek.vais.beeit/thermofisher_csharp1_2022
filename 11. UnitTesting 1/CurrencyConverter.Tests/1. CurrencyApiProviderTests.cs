﻿using BeeIT.UnitTesting.CurrencyConverter.Models;
using BeeIT.UnitTesting.CurrencyConverter.Tests.Mocks;
using NUnit.Framework;
using System.Text.Json;

namespace BeeIT.UnitTesting.CurrencyConverter.Tests;

public class CurrencyApiProviderTests
{
    private CurrencyApiProvider _currencyApi;
    private HttpMessageHandlerStub _handler;

    [SetUp]
    public void SetUp()
    {
        _handler = new HttpMessageHandlerStub();
        _currencyApi = new CurrencyApiProvider(_handler);
    }

    [TestCase("latest")]
    [TestCase("2022-06-09")]
    public async Task GetAllCurrenciesAsync_FromFile_ShouldReturnCorrectValue(string date)
    {
        var result = LoadFile($"all-{date}.json");
        _handler.SetContent(result);

        var actualCurrencies = await _currencyApi.GetAllCurrenciesAsync(CancellationToken.None);
        var expectedCurrencies = JsonSerializer.Deserialize<Dictionary<string, string>>(result)
            .Select(x => new Currency { Code = x.Key, Name = x.Value});

        Assert.That(actualCurrencies, Is.EquivalentTo(expectedCurrencies));
        Assert.That(_handler.HttpRequestUrl, Is.EqualTo("https://cdn.jsdelivr.net/gh/fawazahmed0/currency-api@1/latest/currencies.json"));    
    }

    [TestCase("EUR", "JPY", "latest", 140.920194)]
    [TestCase("USD", "CAD", "2022-03-04", 1.267295)]
    public async Task GetRateAsync_FromFile_ShouldReturnCorrectRate(string currency1, string currency2, string date, double expectedRate)
    {
        // Napište test pro GetRateAsync 
        // 1. Načti soubor rate-EUR-JPY-latest.json nebo rate-USD-CAD-2022-03-04.json
        // 2. Nastav Stub, aby vracel obsah načteného souboru
        // 3. Zavolej metodu GetRateAsync
        // 4. Otestuj vrácenou hodnotu s očekávanou expectedRate
        // 5. Otestuj zavolaný http request s hodnotou https://cdn.jsdelivr.net/gh/fawazahmed0/currency-api@1/latest/currencies/EUR/JPY.json
    }

    private string LoadFile(string filename)
    {
        var assembly = GetType().Assembly;
        var resourceName = $"{GetType().Namespace}.Responses.{filename}";

        using (var stream = assembly.GetManifestResourceStream(resourceName))
        using (var reader = new StreamReader(stream))
        {
            return reader.ReadToEnd();
        }
    }
}
