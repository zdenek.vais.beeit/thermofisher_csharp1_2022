﻿using System.Net;

namespace BeeIT.UnitTesting.CurrencyConverter.Tests.Mocks;

internal class HttpMessageHandlerStub : HttpMessageHandler
{
    public HttpResponseMessage HttpResponseMessage { get; set; }
    public string HttpRequestUrl { get; private set; }

    public HttpMessageHandlerStub()
    {
        HttpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
        {
            Content = new StringContent("")
        };
    }

    public void SetContent(string content)
    {
        HttpResponseMessage.Content = new StringContent(content);
    }

    protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
    {
        cancellationToken.ThrowIfCancellationRequested();
        HttpRequestUrl = request.RequestUri.ToString();
        return Task.FromResult(HttpResponseMessage);
    }
}
