﻿using BeeIT.UnitTesting.CurrencyConverter.Interfaces;
using BeeIT.UnitTesting.CurrencyConverter.Models;
using Moq;
using NUnit.Framework;

namespace BeeIT.UnitTesting.CurrencyConverter.Tests;

public class CurrencyConverterTests
{
    private ICurrencyRateConverter _converter;
    private ICurrencyProvider _currencyApiProvider;

    [SetUp]
    public void SetUp()
    {
        _currencyApiProvider = Mock.Of<ICurrencyProvider>();
        _converter = new CurrencyRateConverter(_currencyApiProvider);
    }

    [TestCase("ABC")]
    [TestCase("DEF")]
    public void ConvertAsync_NonExistingCurrency_ShouldThrowArgumentException(string currencyCode)
    {
        Mock.Get(_currencyApiProvider)
            .Setup(x => x.GetAllCurrenciesAsync(It.IsAny<CancellationToken>()))
            .ReturnsAsync(new Currency[]
            {
                new Currency
                {
                    Code = "Non existing",
                },
            });
        Assert.That(() => _converter.ConvertAsync(1, currencyCode, "A", CancellationToken.None), Throws.ArgumentException);
    }

    [TestCase("USD", "CAD", 1.2, 50, 60)]
    [TestCase("GBP", "CZK", 123.245, 60, 7394.7)]
    [TestCase("AUD", "PLN", 0.14, 70, 9.8)]
    [TestCase("EUR", "MUR", 0.87, 80, 69.6)]
    [TestCase("SOS", "VET", 5.69, 90, 512.1)]
    public async Task ConvertAsync_GivenRates_ShouldReturnCorrectValue(string sourceCurrency, string destinationCurrency, double expectedRate, double amount, double expectedResult)
    {
        // Napište test pro ConvertAsync
        // 1. Nastavte mock _currencyApiProvider na metodu GetAllCurrenciesAsync,
        //      aby vracela seznam dvou tříd Currency [sourceCurrency, destinationCurrency]
        // 2. Nastavte mock _currencyApiProvider na metodu GetRateAsync,
        //      aby vracela hodnotu expectedRate
        // 3. Zavolej metodu ConvertAsync
        // 4. Otestuj návratovou hodnotu ConvertAsync s hodnotou expectedResult
    }

    [Test]
    public void ConvertAsync_NullCurrency_ShouldThrowArgumentException()
    {
        Assert.That(() => _converter.ConvertAsync(1, null, "A", CancellationToken.None), Throws.ArgumentException);
        Assert.That(() => _converter.ConvertAsync(1, "A", null, CancellationToken.None), Throws.ArgumentException);
    }

    [TestCase(0)]
    [TestCase(-1)]
    [TestCase(-2)]
    public void ConvertAsync_NegativeAmount_ShouldThrowArgumentException(double amount)
    {
        Assert.That(() => _converter.ConvertAsync(amount, "A", "B", CancellationToken.None), Throws.ArgumentException);
    }

    [Test]
    public void ConvertAsync_TwoCalls_ShouldLoadCurrencyCodesOnlyOnce()
    {
        var sourceCurrency = "A";
        var destinationCurrency = "B";
        Mock.Get(_currencyApiProvider)
            .Setup(x => x.GetAllCurrenciesAsync(It.IsAny<CancellationToken>()))
            .ReturnsAsync(new Currency[]
            {
                new Currency
                {
                    Code = sourceCurrency,
                },
                new Currency
                {
                    Code = destinationCurrency,
                },
            });

        _converter.ConvertAsync(1, "A", "B", CancellationToken.None);
        _converter.ConvertAsync(1, "A", "B", CancellationToken.None);

        Mock.Get(_currencyApiProvider)
            .Verify(x => x.GetAllCurrenciesAsync(It.IsAny<CancellationToken>()), Times.Once());
    }
}
