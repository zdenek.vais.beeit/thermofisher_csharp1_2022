﻿using BeeIT.UnitTesting.CurrencyConverter.Models;
using BeeIT.UnitTesting.CurrencyConverter.Tests.Mocks;
using NUnit.Framework;
using System.IO.Abstractions;
using System.IO.Abstractions.TestingHelpers;
using System.Reflection.Metadata;
using System.Text.Json;

namespace BeeIT.UnitTesting.CurrencyConverter.Tests;

public class CurrencyFileProviderTests
{
    private const string BASE_PATH = "C:\\";
    private CurrencyFileProvider _currencyProvider;
    private IFileSystem _fileSystem;

    [OneTimeSetUp]
    public void Initialize()
    {
        _fileSystem = new MockFileSystem();
        var files = new[]
        {
            "all-latest.json",
            "all-2022-06-09.json",
            "rate-EUR-JPY-latest.json",
            "rate-USD-CAD-2022-03-04.json",
        };
        foreach (var file in files)
        {
            _fileSystem.File.WriteAllText($"C:\\{file}", LoadFile(file));
        }
    }

    [SetUp]
    public void SetUp()
    {
        _currencyProvider = new CurrencyFileProvider(_fileSystem, BASE_PATH);
    }

    [TestCase("latest")]
    public async Task GetAllCurrenciesAsync_FromFile_ShouldReturnCorrectValue(string date)
    {
        var result = LoadFile($"all-{date}.json");
        var actualCurrencies = await _currencyProvider.GetAllCurrenciesAsync(CancellationToken.None);
        var expectedCurrencies = JsonSerializer.Deserialize<Dictionary<string, string>>(result)
            .Select(x => new Currency { Code = x.Key, Name = x.Value });

        Assert.That(actualCurrencies, Is.EquivalentTo(expectedCurrencies));
    }

    // Otestujte metodu GetRateAsync

    private string LoadFile(string filename)
    {
        var assembly = GetType().Assembly;
        var resourceName = $"{GetType().Namespace}.Responses.{filename}";

        using (var stream = assembly.GetManifestResourceStream(resourceName))
        using (var reader = new StreamReader(stream))
        {
            return reader.ReadToEnd();
        }
    }
}
