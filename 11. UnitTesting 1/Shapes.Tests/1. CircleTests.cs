﻿using NUnit.Framework;

namespace BeeIT.UnitTesting.Shapes.Tests;

//====================================================================================================================
// 1. Opravte třídu Circle tak, aby prošly všechny testy
//====================================================================================================================
public class CircleTests
{
    private Circle _circle;

    [SetUp]
    public void SetUp()
    {
        _circle = new Circle(5);
    }

    [TestCase(0)]
    [TestCase(-1)]
    [TestCase(-10)]
    [TestCase(-50)]
    public void Constructor_NegativeValue_ShouldThrowArgumentOutOfRangeException(double radius)
    {
        Assert.That(() => new Circle(radius), Throws.TypeOf<ArgumentOutOfRangeException>());
    }

    [TestCase(1)]
    [TestCase(3)]
    [TestCase(10)]
    public void SetRadius_ValidValue_ShouldUpdateRadiusAndDiameter(double radius)
    {
        _circle.Radius = radius;

        Assert.That(_circle.Radius, Is.EqualTo(radius));
        Assert.That(_circle.Diameter, Is.EqualTo(radius * 2));
    }

    [TestCase(0)]
    [TestCase(-1)]
    [TestCase(-10)]
    [TestCase(-50)]
    public void SetRadius_NegativeValue_ShouldThrowArgumentOutOfRangeException(double radius)
    {
        Assert.That(() => _circle.Radius = radius, Throws.TypeOf<ArgumentOutOfRangeException>());
    }

    [TestCase(3)]
    [TestCase(5)]
    [TestCase(15)]
    public void SetDiameter_ValidValue_ShouldUpdateRadiusAndDiameter(double diameter)
    {
        _circle.Diameter = diameter;

        Assert.That(_circle.Radius, Is.EqualTo(diameter / 2));
        Assert.That(_circle.Diameter, Is.EqualTo(diameter));
    }

    [TestCase(0)]
    [TestCase(-2)]
    [TestCase(-15)]
    [TestCase(-25)]
    public void SetDiameter_NegativeValue_ShouldThrowArgumentOutOfRangeException(double diameter)
    {
        Assert.That(() => _circle.Diameter = diameter, Throws.TypeOf<ArgumentOutOfRangeException>());
    }

    [TestCase(1)]
    [TestCase(2)]
    [TestCase(3)]
    [TestCase(5)]
    public void GetArea_ValidValue_ShouldCalculateArea(double radius)
    {
        _circle.Radius = radius;
        Assert.That(_circle.Area, Is.EqualTo(Math.PI * radius * radius));
    }
    
    [TestCase(90)]
    [TestCase(180)]
    [TestCase(270)]
    [TestCase(360)]
    public void GetSectorArea_ValidAngle_ShouldCalculateSectorArea(double angle)
    {
        Assert.That(_circle.GetSectorArea(angle), Is.EqualTo(_circle.Area * angle / 360));
    }

    [TestCase(0)]
    [TestCase(-50)]
    [TestCase(400)]
    public void GetSectorArea_InvalidAngle_ShouldThrowArgumentOutOfRangeException(double angle)
    {
        Assert.That(() => _circle.GetSectorArea(angle), Throws.TypeOf<ArgumentOutOfRangeException>());
    }
}
