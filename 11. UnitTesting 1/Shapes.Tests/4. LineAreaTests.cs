﻿using BeeIT.UnitTesting.Shapes.Common;
using NUnit.Framework;

namespace BeeIT.UnitTesting.Shapes.Tests;

//====================================================================================================================
// 4. Naimplementujte třídu LineArea tak, aby prošly všechny testy
//      Tuto testovací třídu neměňte
//      Příklad TTD - Test Driven Development
//====================================================================================================================
public class LineAreaTests
{
    private LineArea _lineArea;

    [SetUp]
    public void SetUp()
    {
        _lineArea = new LineArea();
    }

    public static List<Point>[] Lines =
    {
        new List<Point> { 
            Point.Create(0, 0),
            Point.Create(1, 0),
            Point.Create(1, 1), 
        },
        new List<Point> {
            Point.Create(0, 0),
            Point.Create(1, 0),
            Point.Create(1, 1),
            Point.Create(0, 1),
        },
    };

    public static List<Point>[] TooShortLines =
    {
        new List<Point> {
        },
        new List<Point> {
            Point.Create(0, 0),
        },
        new List<Point> {
            Point.Create(0, 0),
            Point.Create(1, 0),
        },
    };

    public static List<Point>[] OverlappingLines =
    {
        new List<Point> {
            Point.Create(0, 0),
            Point.Create(1, 0),
            Point.Create(1, 0),
        },
        new List<Point> {
            Point.Create(0, 0),
            Point.Create(0, 0),
            Point.Create(1, 0),
        },
    };

    public static TestCaseData[] LinesWithCircumference =
    {
        new TestCaseData(
            new List<Point> {
                Point.Create(0, 0),
                Point.Create(1, 0),
                Point.Create(1, 1),
                Point.Create(0, 1),
            },
            4),
    };

    [TestCaseSource(nameof(Lines))]
    public void SetLine_ValidValues_ShouldUpdatePoints(List<Point> line)
    {
        _lineArea.Line = line;
        Assert.That(_lineArea.Line, Is.EquivalentTo(line));
    }

    [TestCaseSource(nameof(TooShortLines))]
    public void SetLine_TooShortLines_ShouldThrowArgumentOutOfRangeException(List<Point> line)
    {
        Assert.That(() => _lineArea.Line = line, Throws.TypeOf<ArgumentOutOfRangeException>());
    }

    [TestCaseSource(nameof(OverlappingLines))]
    public void SetLine_OverlappingLines_ShouldThrowArgumentException(List<Point> line)
    {
        Assert.That(() => _lineArea.Line = line, Throws.TypeOf<ArgumentException>());
    }

    [TestCaseSource(nameof(Lines))]
    public void SetPoint_ValidValues_ShouldUpdatePoint(List<Point> line)
    {
        var p = Point.Create(10, 20);
        var index = 1;

        _lineArea.Line = line;
        _lineArea.SetPoint(index, p);

        Assert.That(_lineArea.Line[index], Is.EqualTo(p));
    }

    [TestCase(-1)]
    [TestCase(-10)]
    [TestCase(2)]
    [TestCase(3)]
    [TestCase(10)]
    public void SetPoint_OutOfRangeIndex_ShouldThrowArgumentOutOfRangeException(int index)
    {
        Assert.That(() => _lineArea.SetPoint(index, Lines[0][0]), Throws.TypeOf<ArgumentOutOfRangeException>());
    }

    [Test]
    public void SetPoint_OverlappingPoint_ShouldThrowArgumentException()
    {
        _lineArea.Line = Lines[0].ToList();
        Assert.That(() => _lineArea.SetPoint(1, Lines[0][0]), Throws.TypeOf<ArgumentException>());
    }

    [TestCaseSource(nameof(LinesWithCircumference))]
    public void GetCircumference_ValidValues_ShouldReturnCorrectValue(List<Point> line, double circumference)
    {
        _lineArea.Line = line;
        Assert.That(_lineArea.GetCircumference(), Is.EqualTo(circumference));
    }

    [Test]
    public void GetCircumference_InitialValue_ShouldReturnZero()
    {
        Assert.That(_lineArea.GetCircumference(), Is.EqualTo(0));
    }
}
