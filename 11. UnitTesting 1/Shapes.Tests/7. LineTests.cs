﻿using BeeIT.UnitTesting.Shapes.Common;
using NUnit.Framework;

namespace BeeIT.UnitTesting.Shapes.Tests;

//====================================================================================================================
// 6. Napište testy pro třídu Cylinder a opravte její chybu
//====================================================================================================================
public class LineTests
{
    private Line _line;

    public static IEnumerable<TestCaseData> VectorsWithAngle
    {
        get
        {
            for (var angle = 0.0; angle < 90; angle += 10.0)
            {
                var angleRad = angle / 180.0 * Math.PI;
                var leftRight = angle > 90 && angle < 270 ? -1 : 1;
                var upDown = angle > 0 && angle < 180 ? 1 : -1;
                yield return new TestCaseData(
                    Point.Create(
                        Math.Cos(angleRad), 
                        Math.Sin(angleRad)), 
                    angle);
            }
        }
    }

    [SetUp]
    public void SetUp()
    {
        _line = new Line
        {
            PointA = Point.Create(1, 1),
            PointB = Point.Create(2, 2),
        };
    }

    [TestCaseSource(nameof(VectorsWithAngle))]
    public void GetAngle_ValidValue_ShouldReturnAngle(Point p, double angle)
    {
        _line.PointA = Point.Create(0, 0);
        _line.PointB = p;
        Assert.That(_line.Angle, Is.EqualTo(angle).Within(0.01));
    }
}
