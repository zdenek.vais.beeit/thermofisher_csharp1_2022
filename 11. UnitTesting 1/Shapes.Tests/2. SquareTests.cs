﻿using NUnit.Framework;

namespace BeeIT.UnitTesting.Shapes.Tests;

//====================================================================================================================
// 2. Napište testy pro třídu Square a opravte její chybu
//====================================================================================================================
public class SquareTests
{
    private Square _square;

    [SetUp]
    public void SetUp()
    {
        _square = new Square(2);
    }

    [TestCase(0)]
    [TestCase(-1)]
    [TestCase(-10)]
    [TestCase(-50)]
    public void Constructor_NegativeValue_ShouldThrowArgumentOutOfRangeException(double length)
    {
        Assert.Fail();
    }

    [TestCase(1)]
    [TestCase(3)]
    [TestCase(10)]
    public void SetLength_ValidValue_ShouldUpdateLength(double length)
    {
        Assert.Fail();
    }

    [TestCase(0)]
    [TestCase(-1)]
    [TestCase(-10)]
    [TestCase(-50)]
    public void SetLength_NegativeValue_ShouldThrowArgumentOutOfRangeException(double length)
    {
        Assert.Fail();
    }

    [TestCase(3)]
    [TestCase(5)]
    [TestCase(15)]
    public void GetArea_ValidLength_ShouldCalculateArea(double length)
    {
        Assert.Fail();
    }

    [TestCase(1, 1.41)]
    [TestCase(2, 2.82)]
    [TestCase(3, 4.24)]
    public void GetDiagonal_ValidLength_ShouldUpdateDiagonal(double length, double expectedDiagonal)
    {
        Assert.Fail();

        // Porovnani rovnosti s toleranci:
        // Assert.That(1, Is.EqualTo(1.01).Within(0.01));
    }
}
