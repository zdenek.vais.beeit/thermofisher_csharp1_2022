﻿using BeeIT.UnitTesting.CurrencyConverter.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using System;

namespace BeeIT.UnitTesting.CurrencyConverter.Interfaces
{
    public interface ICurrencyProvider
    {
        Task<IEnumerable<Currency>> GetAllCurrenciesAsync(CancellationToken? token = null);
        Task<double> GetRateAsync(string fromCurrencyCode, string toCurrencyCode, DateTime? date = null, CancellationToken? token = null);
    }
}
