﻿using System.Threading.Tasks;
using System.Threading;

namespace BeeIT.UnitTesting.CurrencyConverter.Interfaces
{
    public interface ICurrencyRateConverter
    {
        Task<double> ConvertAsync(double amount, string currencyFrom, string currencyTo, CancellationToken? token = null);
    }
}
