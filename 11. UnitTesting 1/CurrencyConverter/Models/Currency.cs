﻿using System;
using System.Diagnostics;

namespace BeeIT.UnitTesting.CurrencyConverter.Models
{
    [DebuggerDisplay("{Code}: {Name}")]
    public class Currency : IEquatable<Currency>
    {
        public string Code { get; set; }
        public string Name { get; set; }

        public bool Equals(Currency other)
        {
            return other is { } && Code == other.Code && Name == other.Name;
        }
    }
}
