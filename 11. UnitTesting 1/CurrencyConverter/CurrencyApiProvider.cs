﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text.Json;
using System.Linq;
using BeeIT.UnitTesting.CurrencyConverter.Models;
using System;
using System.Net.Http.Json;
using BeeIT.UnitTesting.CurrencyConverter.Interfaces;

namespace BeeIT.UnitTesting.CurrencyConverter
{
    public class CurrencyApiProvider : ICurrencyProvider
    {
        private const string CURRENCY_API_URL = "https://cdn.jsdelivr.net/gh/fawazahmed0/currency-api@{0}/{1}/{2}.json";
        private const string CURRENCY_API_VERSION = "1";
        private const string CURRENCY_API_DATE_LATEST = "latest";
        private const string CURRENCY_API_ENDPOINT_LIST = "currencies";
        private const string CURRENCY_API_ENDPOINT_CONVERT = "currencies/{0}/{1}";
        private readonly HttpClient _client;

        public CurrencyApiProvider(HttpMessageHandler handler = null)
        {
            _client = new HttpClient(handler ?? new HttpClientHandler());
            _client.DefaultRequestHeaders.Accept.Clear();
        }

        public async Task<IEnumerable<Currency>> GetAllCurrenciesAsync(CancellationToken? token = null)
        {
            var currenciesDict = await _client.GetFromJsonAsync<Dictionary<string, string>>(
                string.Format(CURRENCY_API_URL, CURRENCY_API_VERSION, CURRENCY_API_DATE_LATEST, CURRENCY_API_ENDPOINT_LIST), token ?? CancellationToken.None);
            return currenciesDict.Select(x => new Currency { Code = x.Key, Name = x.Value });
        }

        public async Task<double> GetRateAsync(string fromCurrencyCode, string toCurrencyCode, DateTime? date = null, CancellationToken? token = null)
        {
            fromCurrencyCode = fromCurrencyCode.ToLower();
            toCurrencyCode = toCurrencyCode.ToLower();
            var convertEndpoint = string.Format(CURRENCY_API_ENDPOINT_CONVERT, fromCurrencyCode, toCurrencyCode);
            var currenciesDict = await _client.GetFromJsonAsync<Dictionary<string, object>>(
                    string.Format(CURRENCY_API_URL, CURRENCY_API_VERSION,
                    date?.ToString("yyyy-MM-dd") ?? CURRENCY_API_DATE_LATEST, 
                    convertEndpoint), 
                    token ?? CancellationToken.None);
            var rate = (JsonElement)currenciesDict[toCurrencyCode];
            return rate.GetDouble();
        }
    }
}