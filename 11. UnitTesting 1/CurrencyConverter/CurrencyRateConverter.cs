﻿using BeeIT.UnitTesting.CurrencyConverter.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BeeIT.UnitTesting.CurrencyConverter
{
    public class CurrencyRateConverter : ICurrencyRateConverter
    {
        private readonly ICurrencyProvider _currencyApiProvider;
        private IEnumerable<string> _currencyCodes;

        public CurrencyRateConverter(
            ICurrencyProvider currencyApiProvider
            )
        {
            _currencyApiProvider = currencyApiProvider;
        }

        public async Task<double> ConvertAsync(double amount, string currencyFrom, string currencyTo, CancellationToken? token = null)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("Amound to convert has to be positive!");
            }

            await VerifyCurrencyAsync(currencyFrom, currencyTo);
            var rate = await _currencyApiProvider.GetRateAsync(currencyFrom, currencyTo, null, token ?? CancellationToken.None);
            return amount * rate;
        }

        private async Task VerifyCurrencyAsync(params string[] currencies)
        {
            if (_currencyCodes == null )
            {
                _currencyCodes = (await _currencyApiProvider.GetAllCurrenciesAsync(CancellationToken.None))
                    .Select(x => x.Code);
            }

            foreach (var currencyCode in currencies) 
            {
                if (!_currencyCodes.Contains(currencyCode))
                {
                    throw new ArgumentException($"Wrong currency {currencyCode}!");
                }
            }
        }
    }
}
