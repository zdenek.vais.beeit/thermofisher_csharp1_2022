﻿using BeeIT.UnitTesting.CurrencyConverter.Interfaces;
using BeeIT.UnitTesting.CurrencyConverter.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;

namespace BeeIT.UnitTesting.CurrencyConverter
{
    public class CurrencyFileProvider : ICurrencyProvider
    {
        private readonly IFileSystem _fileSystem;
        private readonly string _basePath;

        public CurrencyFileProvider(
            IFileSystem fileSystem,
            string basePath
            )
        {
            _fileSystem = fileSystem;
            _basePath = basePath;
        }

        public async Task<IEnumerable<Currency>> GetAllCurrenciesAsync(CancellationToken? token = null)
        {
            var path = Path.Combine(_basePath, "all-latest.json");
            var currenciesFile = await _fileSystem.File.ReadAllTextAsync(path, token ?? CancellationToken.None);
            var currenciesDict = JsonSerializer.Deserialize<Dictionary<string, string>>(currenciesFile);
            return currenciesDict.Select(x => new Currency { Code = x.Key, Name = x.Value });
        }

        public async Task<double> GetRateAsync(string fromCurrencyCode, string toCurrencyCode, DateTime? date = null, CancellationToken? token = null)
        {
            var dateFormatted = date?.ToString("yyyy-MM-dd") ?? "latest";
            var path = Path.Combine(_basePath, $"rate-{fromCurrencyCode}-{toCurrencyCode}-{dateFormatted}.json");
            var rateFile = await _fileSystem.File.ReadAllTextAsync(path, token ?? CancellationToken.None);
            var currenciesDict = JsonSerializer.Deserialize<Dictionary<string, object>>(rateFile);
            var rate = (JsonElement)currenciesDict[toCurrencyCode.ToLower()];
            return rate.GetDouble();
        }
    }
}
